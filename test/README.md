# Test Suites

There are currently two main test suites: a suite of internal, stand-alone tests
and a suite of tests run against the Jane Street code base.

To run these test suites, run make in the top level directory. This is necessary
to ensure that appropriate environment variables are set. The targets for
running the tests have been implemented so as to use a period-separated
namespacing scheme. To run all the test suites, run the following:

```[bash]
> make tests.all
```

For each test, a log file is generated as well as a diff patch containing the
results of the refactoring.

Artifacts can be cleaned from the test suite directories using:

```[bash]
> make tests.clean
```

## Internal Test Suite

To run all the unit tests in the internal suite, run the following from ROTOR's
top level directory:

```[bash]
> make tests.internal.all
```

This will run the following collections of tests (which can also be run
individually by using their names as targets for `make`):

    tests.internal.rename_val

Artifacts (i.e. log files, patch files,error output logs, and build artifacts)
can be cleaned from the whole internal suite or from one test collection
subdirectory using the following targets:

    tests.internal.clean
    tests.internal.[test-name].clean

## Jane Street Test Suite

The tests in this suite run against a version of a subset of the Jane Street
public codebase. This version is included in ROTOR's repository as
`test/jane-street/js-testbed.tar.gz`. To use it in the test suite, extract this
archive and make its location available in an environment variable called
`JANE_STREET_PATH`.

Before running the Jane Street test suite, compile the source using the jbuilder
command:

```[bash]
> jbuilder build
```

After updating your OCaml environment (e.g. `opam upgrade`) the test suite
source may need to be recompiled, otherwise you may encounter errors when
running ROTOR over the test suite (e.g. `Ocaml_typing__Env.Error` exceptions).

To run all the tests in the Jane Street suite, run the following from ROTOR's
top level directory:

```[bash]
> make tests.jane-street.all
```

This will run the following collections of tests (which can also be run
individually by using their names as targets for `make`):

    tests.jane-street.rename_val

Artifacts (i.e. log files, patch files, and error output logs) can be cleaned
from the whole Jane Street suite or from one test collection subdirectory using
the following targets:

    tests.jane-street.clean
    tests.jane-street.[test-name].clean

### Running ROTOR Manually on the Jane Street Test Suite

For convenience, you may source the shell script

    utils/scripts/load_js_params.sh

This will set two environment variables, `INCLUDE_PARAMS` and `INPUT_DIRS`,
which contain, respectively, the necessary command line directives to make ROTOR
aware of the directories comprising the binary (`.cmt` and `.cmti`) and source
files of the Jane Street test suite.

A file containing the pre-computed file dependencies for the Jane Street test
suite is also provided (`test/jane-street/filedeps`), which can be loaded by
ROTOR using the `-get-deps-from-file` command line flag. ROTOR will compute the
file dependencies at runtime, however this may take quite a while (on the order
of tens of seconds).

For example, you may run a renaming refactoring over the Jane Street test suite
as follows:

```[bash]
> cd $JANE_STREET_PATH
> rotor rename $INCLUDE_PARAMS $INPUT_DIRS --deps-from /path/to/rotor/test/jane-street/filedeps base.Array:fold foo
```