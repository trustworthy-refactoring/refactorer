Unit Tests for "Rename Value"
----

<dl>
  <dt>01</dt>
  <dd>Rename function foo in Test module.</dd>
  <dt>02</dt>
  <dd>Rename function foo in Test module.
      Both implementation and interface contain two bindings of the identifier
      'foo', only one of which should be modified.
  </dd>
  <dt>03</dt>
  <dd>Rename function hd in Test module.
      Both interface and implementation open a module (List) which shadows the
      'hd' identifier.
  </dd>
  <dt>04</dt>
  <dd>Rename function foo in Test module to 'bar' resulting in shadowing of
      existing binding. Failure expected.
  </dd>
  <dt>05</dt>
  <dd>Rename function baz in module Test which contains no binding for baz.
      Failure expected.
  </dd>
  <dt>06</dt>
  <dd>Rename function foo in module A, where A is included by module B thus
      requiring B.foo to be renamed as well.
  </dd>
</dl>