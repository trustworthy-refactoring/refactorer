OCAMLDOC := ocamldoc -hide-warnings
THREADS := 8
OCB := ocamlbuild -use-ocamlfind -cflag -g -cflag -bin-annot -j $(THREADS) -ocamldoc "$(OCAMLDOC)"

ROOT := $(shell pwd)

BUILD_DIR := _build

OCAML_PATH := $(strip $(shell opam config var lib || echo -n ""))
OCAML_PATH := $(if $(OCAML_PATH),$(OCAML_PATH),$(shell (ocamlfind printconf path || echo -n "") | (read dir; echo -n "$$dir")))

MAIN_MODULE := main
MAIN_TARGET := src/driver/$(MAIN_MODULE).native
MAIN_EXE := $(BUILD_DIR)/$(MAIN_TARGET)
MAIN_TOOLNAME := rotor

PT_PRINTER_UTIL := utils/parsetree_printer.native
TT_PRINTER_UTIL := utils/typedtree_printer.native

LIB := utils/refactor_lib.cma
VISITORS_LIB := utils/ast_visitors.cma

export ROOT BUILD_DIR MAIN_EXE OCAML_PATH

.PHONY: main rename_main_exe utils refactor_lib clean

%.native:
	$(OCB) $@

%.byte:
	$(OCB) $@

all: main utils refactor_lib

main: $(MAIN_TARGET)
	if [ -e "$(MAIN_MODULE).native" ]; then \
		mv $(MAIN_MODULE).native $(MAIN_TOOLNAME); \
	fi

utils: $(PT_PRINTER_UTIL) $(TT_PRINTER_UTIL)

refactor_lib:
	$(OCB) $(LIB)

visitors_lib:
	$(OCB) $(VISITORS_LIB)

clean:
	$(OCB) -clean

tests.%:
	$(MAKE) -C test --no-print-directory $*