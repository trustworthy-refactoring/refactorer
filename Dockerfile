FROM ubuntu:latest

MAINTAINER Reuben N. S. Rowe (r.n.s.rowe@kent.ac.uk)

ARG DEBIAN_FRONTEND=noninteractive

ENV TERM linux

RUN apt-get update && apt-get install -y --no-install-recommends \
  camlp4-extra \
  git \
  libpcre3-dev \
  m4 \
  ocaml \
  ocaml-native-compilers \
  opam \
  patchutils \
  pkg-config \
  rlwrap \
  software-properties-common \
&& rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y --no-install-recommends curl && \
  (curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash) && \
  apt-get update && \
  apt-get install -y --no-install-recommends git-lfs && \
  apt-get remove -y curl && \
  rm -rf /var/lib/apt/lists/*

RUN useradd -m rotor

USER rotor

RUN opam init --auto-setup --comp 4.04.2
RUN opam install -y jbuilder.1.0+beta10 containers ocamlgraph logs visitors re pcre mparser

WORKDIR /home/rotor

RUN git clone https://gitlab.com/trustworthy-refactoring/devcompiler.git && \
  cd devcompiler && \
  git checkout refactoring_dev && \
  eval `opam config env` && \
  jbuilder build devcompiler.install && \
  opam-installer _build/default/devcompiler.install --prefix ~/.opam/4.04.2/

WORKDIR /home/rotor

RUN git clone https://gitlab.com/trustworthy-refactoring/refactorer && \
  cd refactorer && \
  eval `opam config env` && \
  make

WORKDIR /home/rotor

RUN eval `opam config env`; \
  mkdir jane-street-testbed && \
  tar -xzf ./refactorer/test/jane-street/js-testbed.tar.gz -C jane-street-testbed && \
  cd jane-street-testbed && \
  jbuilder build

ENV JANE_STREET_PATH /home/rotor/jane-street-testbed

WORKDIR /home/rotor/refactorer

CMD [ "/bin/bash", "-l" ]

