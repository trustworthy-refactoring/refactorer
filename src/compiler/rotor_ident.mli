include module type of struct include Compiler_import.Ident end

val with_name : t -> string -> t
(** Replace the name of the identifier without changing the binding stamp or
    flags. *)