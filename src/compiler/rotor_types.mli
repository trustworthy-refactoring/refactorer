open Compiler_import

include module type of struct include Types end

val get_module_type_path : module_type -> Path.t