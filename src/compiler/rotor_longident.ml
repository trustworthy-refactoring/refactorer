open Containers

open Compiler_import

include Longident

let build xs =
  List.fold_left
    (fun p m -> Ldot (p, m))
    (Lident (List.hd xs))
    (List.tl xs)

let hd lid = List.hd (flatten lid)
let tl lid = build (List.tl (flatten lid))

let rec append lid =
  function
  | Lident id -> Ldot(lid, id)
  | Ldot(lid', id) -> Ldot(append lid lid', id)
  | Lapply _ -> invalid_arg (Format.sprintf "%s.append" __MODULE__)
