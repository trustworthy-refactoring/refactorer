include module type of Compiler_import.Env

val normalize_module_path : 
  Compiler_import.Location.t option -> t -> 
    Compiler_import.Path.t -> Compiler_import.Path.t

val normalize_mty_path :
  ?lax:bool -> t -> Compiler_import.Path.t -> Compiler_import.Path.t