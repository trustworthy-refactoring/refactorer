(******************************************************************************)
(* The interface to the compiler                                              *)
(******************************************************************************)

include module type of Compiler_import [@remove_aliases]
          with module Env       := Rotor_env
           and module Ident     := Rotor_ident
           and module Location  := Rotor_location
           and module Longident := Rotor_longident
           and module Path      := Rotor_path
           and module Types     := Rotor_types
           and module Typedtree := Rotor_typedtree
           and module Typemod   := Typemod

module Env       = Rotor_env
module Ident     = Rotor_ident
module Location  = Rotor_location
module Longident = Rotor_longident
module Path      = Rotor_path
module Types     = Rotor_types
module Typedtree = Rotor_typedtree

module Typemod : sig
  include module type of Compiler_import.Typemod
  val type_interface : 
    string -> Env.t -> Parsetree.signature -> Typedtree.signature
end

val find_in_path : string -> string
val find_in_path_uncap : string -> string

val init_path : ?dir:string -> unit -> unit