(******************************************************************************)
(* The interface to the compiler                                              *)
(******************************************************************************)

include module type of Ocaml_utils
include module type of Ocaml_parsing
include module type of Ocaml_typing
include module type of Ocaml_driver
