(** A module to represent the result of applying a refactoring to an AST. *)
open Containers
open Compiler

type t
(** The abstract type of replacements. *)

type payload = string
(** The payload of the replacement. *)
(*  TODO: Do we need a more structured payload that just a string? *)

module Set :
sig
    include Set.S with type elt = t
    class monoid :
      object
        method private zero : t
        method private plus : t -> t -> t
      end
    (** A class that represents the Set monoid at the type of replacements,
        which can be inherited by reduce visitors. *)

    val of_opt : elt option -> t
    (** [of_opt x] returns [empty] if x is [None] and [singleton v] if x is
        [Some v]. *)
end
(** Sets of replacements *)

val compare : t -> t -> int
(** Implements an ordering on replacements which determines when one replacement
    should be carried out before another. *)

val mk : Location.t -> payload -> t
(** [mk loc s] creates a replacement that replaces the string at location [loc]
    with the string [s]. *)

val mk_opt : Location.t -> payload -> t option
(** [mk_opt loc s] returns [None] if [loc] is a ghost location, otherwise it
    returns [Some r] where [r] is the replacement returned by [mk loc s]. *)

val location : t -> Location.t
(** [location r] returns the location that will be replaced when applying [r]. *)

val apply : t -> string -> string
(** [apply r s] applies the replacement [r] to the string [s] *)

val apply_all : Set.t -> string -> string
(** [apply_all rs s] applies the all replacements in the set [rs] to the string [s] *)

val pp : t Format.printer
(** A pretty-printer for replacements. *)