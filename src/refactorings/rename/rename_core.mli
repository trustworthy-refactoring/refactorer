module ParameterState : sig

  module type S = sig
    type kind_t
    val kind : kind_t Elements.Base.t
    val initialise : string list -> unit
    val init : kind_t Identifier.t * string -> unit
    val from_lib : unit -> string option
    val from_id : unit -> kind_t Identifier.Chain.t
    val to_id : unit -> string
    val to_repr : unit -> Refactoring.Repr.t
    val is_local : Fileinfos.t -> bool
    val kernel_mem : Fileinfos.t -> bool
    val kernel : Codebase.t -> Codebase.elt list
  end

  module Make
    (X : sig
        type kind_t
        val kind : kind_t Elements.Base.t
        val variety : Identifier.Chain.variety
      end)
    : S with type kind_t = X.kind_t
end