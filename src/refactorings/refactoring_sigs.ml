type result = Replacement.Set.t
(** The result of applying the refactoring to an AST. *)

module type Refactoring =
sig

  val name : string
  (** The name of the refactoring. *)

  val initialise : string list -> unit
  (** Initialise the refactoring using command-line arguments. *)

  val process_file : Sourcefile.t -> result
  (** Refactor an input. *)

  val to_repr : unit -> Refactoring_repr.t
  (** The runtime representation of this refactoring *)

  val get_deps :
    mli:Sourcefile.t option -> Sourcefile.t -> Refactoring_deps.Elt.Set.t
  (** [get_deps ~mli f] returns a set of representations of refactorings that
      this refactoring depends on in order to be correct for file [f]. *)

  val kernel : Codebase.t -> Fileinfos.t list
  (** [kernel c] returns the intersection of the kernel of the refactoring with
      the codebase [c].
      Invariant: [kernel c] contains all and only the files [f] in [c] for which
      [kernel_mem f] returns [true]. *)

  val kernel_mem : Fileinfos.t -> bool
  (** [kernel_mem f] returns true if and only if [f] is contained in the kernel
      of this refactoring. *)

end
(** The interface that a refactoring should implement. *)

