open Containers

open Compiler
open Lexing

open Location

type payload = string

module OrderKernel = struct
  type t = {
    location : Location.t ;
    payload  : payload ;
  }
  let compare
        { location = { loc_start = { pos_cnum = p ; _ }; _ }; _ }
        { location = { loc_start = { pos_cnum = p'; _ }; _ }; _ } =
    p - p'
end

module Set =
struct
  include Set.Make(OrderKernel)
  class monoid = object
    inherit [t] VisitorsRuntime.monoid
    method private zero = empty
    method private plus = union
  end
  let of_opt =
    function
    | None ->
      empty
    | Some v ->
      singleton v
  (* TODO: check for consistency when adding elements *)
end

include OrderKernel

let mk loc p = { location = loc; payload = p }
let mk_opt loc p =
  if not loc.loc_ghost then
    Some (mk loc p)
  else
    None

let location { location; _ } = location

let apply { location = { loc_start; loc_end; _ }; payload } s =
  (String.sub s 0 loc_start.pos_cnum)
    ^ payload
    ^ (String.sub s loc_end.pos_cnum ((String.length s) - loc_end.pos_cnum))

let apply_all rs s =
  let () =
    Logging.debug @@ fun _f -> _f
      "Applying replacements to string of length %i" (String.length s) in
  let buf = Buffer.create (String.length s) in
  let cur = ref 0 in
  Set.iter
    (fun { location = { loc_start; loc_end; _ }; payload } ->
      let pos = !cur in
      let () =
        Logging.debug @@ fun _f -> _f
          "Current position is %i, replacing between %i and %i"
          pos loc_start.pos_cnum loc_end.pos_cnum in
      Buffer.add_string buf (String.sub s pos (loc_start.pos_cnum - pos)) ;
      Buffer.add_string buf payload ;
      cur := loc_end.pos_cnum)
    rs ;
  let () = Logging.debug @@ fun _f -> _f "Current position is %i" !cur in
  Buffer.add_string buf (String.sub s !cur ((String.length s) - !cur)) ;
  Buffer.contents buf

let pp fmt { location; payload } =
  Format.fprintf fmt
    "Location: %a, Payload: %s"
    Location.print_compact location payload