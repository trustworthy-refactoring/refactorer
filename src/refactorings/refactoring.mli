open Refactoring_sigs

module Core :
  sig
    (* Nothing here yet! *)
  end
(** A module containing core types and values common to (and importable by)
    all refactorings. *)

module Repr = Refactoring_repr

module Deps = Refactoring_deps

module Identity : Refactoring
(** A module implementing the identity refactoring *)

val mk_repr : string * string list -> Repr.t
(** [mk_repr r params] returns a representation of the refactoring named [r]
    initialised with [params]. Raises [Not_found] if no such refactoring
    exists. *)

val of_repr : Repr.t -> (module Refactoring)
(** [of_repr r] returns a module that performs the refactoring described by
    [r]. *)

val get : string * string list -> (module Refactoring)
(** [get r params] is exactly [of_repr (mk_repr r params)]. *)

val apply :
  Repr.t -> ?filedeps:Fileinfos.Graph.t -> Codebase.t
    -> Deps.t * result Fileinfos.Map.t
(** [apply r cb] applies the refactoring described by [r] to the codebase [cb]
    and returns a pair of the resulting refactoring dependency set and a map
    giving the replacements to be applied to each source file. *)

val apply_all :
  Repr.Set.t -> ?filedeps:Fileinfos.Graph.t -> Codebase.t
    -> Deps.t * result Fileinfos.Map.t
(** [apply_all rs cb] applies the refactorings described by the representations
    in [rs] to the codebase [cb] and returns a pair of the resulting refactoring
    dependency set and a map giving the replacements to be applied to each
    source file. *)