open Containers

open Compiler

open Sourcefile

type result = Replacement.Set.t
(** The result of applying the refactoring to an AST. *)

module Core :
  sig
    (* Nothing here yet! *)
  end
(** A module containing core types and values common to (and importable by)
    all refactorings. *)

module Repr : Refactoring_repr.S

module Deps : Refactoring_deps.S with module Repr := Repr

module type S =
sig

  val name : string
  (** The name of the refactoring. *)

  val initialise : string list -> unit
  (** Initialise the refactoring using command-line arguments. *)

  val process_file : Sourcefile.t -> result
  (** Refactor an input. *)

  val to_repr : unit -> Repr.t
  (** The runtime representation of this refactoring *)

  val get_deps : mli:Sourcefile.t option -> Sourcefile.t -> Deps.Elt.Set.t
  (** [get_deps ~mli f] returns a set of representations of refactorings that
      this refactoring depends on in order to be correct for file [f]. *)

  val kernel : Codebase.t -> Fileinfos.t list
  (** [kernel c] returns the intersection of the kernel of the refactoring with
      the codebase [c].
      Invariant: [kernel c] contains all and only the files [f] in [c] for which
      [kernel_mem f] returns [true]. *)

  val kernel_mem : Fileinfos.t -> bool
  (** [kernel_mem f] returns true if and only if [f] is contained in the kernel
      of this refactoring. *)

end
(** The interface that a refactoring should implement. *)

module Identity : S
(** A module implementing the identity refactoring *)

(* Exceptions *)

(* Errors indicate the irrecoverable failure of a refactoring.
   We follow the pattern found in the compiler of defining a datatype to
   encapsulate the various different kinds of failures that can occur, with
   values wrapped by an Error exception. The module then also provides
   functions for formatting and logging an error.
*)

type error =
  | Not_initialised of string
    (** [Not_initialised msg] indicates that the refactoring has not
        been properly initialised; [msg] provides an error message. *)
  | Bad_sourcefile of string
    (** [Bad_sourcefile msd] indicates that the representation of the source
        code was insufficient to requirements; [msg] provides an error message. *)
  | CompilerError of Location.error
    (** [CompilerError err] indicates that an error was raised by a function in
        the compiler that was used whilst analysis the source representation;
        [err] is the error in question. *)
  | SoundnessViolation of string
    (** [SoundnessViolation msg] indicates that apply the refactoring is
        unsound; [msg] provides a detailed error message. *)


exception Error of error * Printexc.raw_backtrace option
(** An Error indicates the irrecoverable failure of a refactoring. *)

val print_error : error Format.printer
(** [report_error fmt e] reports the error [e] on the formatter [fmt]. *)
val log_error : ?tags:Logs.Tag.set -> error -> unit
(** [log_error e] logs the error on [Logging.log]. *)