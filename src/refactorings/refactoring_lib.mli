open Refactoring

val mk_repr : string * string list -> Repr.t
(** [mk_repr r params] returns a representation of the refactoring named [r]
    initialised with [params]. Raises [Not_found] if no such refactoring
    exists. *)

val of_repr : Repr.t -> (module S)
(** [of_repr r] returns a module that performs the refactoring described by
    [r]. *)

val get : string * string list -> (module S)
(** [get r params] is exactly [of_repr (mk_repr r params)]. *)