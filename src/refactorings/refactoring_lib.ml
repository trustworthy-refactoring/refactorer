open Containers
open Format

open Lib

open Refactoring
open Repr

open Elements.Base
open Identifier

let of_repr = function
| Identity ->
  (module Identity : S)
| Rename ((lib, Chain.Ex (Value, from)), _to) ->
  begin match Chain.variety from with
  | Chain.InImpl ->
    let module R = Rename_val_impl.Make () in
    let () = R.init ((lib, from), _to) in
    (module R : S)
  | Chain.InIntf ->
    let module R = Rename_val_intf.Make () in
    let () = R.init ((lib, from), _to) in
    (module R : S)
  end
| _ ->
  not_implemented ()

let mk_repr (r, params) =
  match r with
  | "rename" ->
    begin match params with
    | [from; _to] ->
      let () = Logging.debug @@ fun _f -> _f
        "Creating representation of \"rename\" with parameters: \"%s\", \"%s\""
        from _to in
      Rename (of_string from, _to)
    | _ ->
      invalid_arg "Wrong number of arguments for [rename]"
    end
  | _ -> raise Not_found

let get (r, params) = of_repr (mk_repr (r, params))