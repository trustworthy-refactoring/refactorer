open Containers

open Compiler

open Sourcefile

open Compiler_utils

open Lib

type result = Replacement.Set.t

module Core : sig end = struct end

module Repr = Refactoring_repr
module Deps = Refactoring_deps

module type S =
sig
  val name : string
  val initialise : string list -> unit
  val process_file : Sourcefile.t -> result
  val to_repr : unit -> Repr.t
  val get_deps : mli:Sourcefile.t option -> Sourcefile.t -> Deps.Elt.Set.t
  val kernel : Codebase.t -> Fileinfos.t list
  val kernel_mem : Fileinfos.t -> bool
end

module Identity : S = struct
  let name = "Identity"
  let initialise _ = ()
  let process_file _ = Replacement.Set.empty
  let to_repr _ = Repr.Identity
  let get_deps ~mli _ = Deps.Elt.Set.empty
  let kernel_mem _ = true
  let kernel _ = []
end

type error =
  | Not_initialised of string
    (** [Not_initialised msg] indicates that the refactoring has not
        been properly initialised; [msg] provides an error message. *)
  | Bad_sourcefile of string
    (** Indicates that the representation of the source code was insufficient to
        requirements. *)
  | CompilerError of Location.error
    (** An error raised by the compiler libraries during the processing of
        environments in the typed tree representation of the source. *)
  | SoundnessViolation of string

let print_error fmt = function
  | Not_initialised s ->
    Format.fprintf fmt "Not initialised! %s" s
  | Bad_sourcefile s ->
    Format.fprintf fmt "Bad sourcefile! %s" s
  | CompilerError e ->
    print_error fmt e
  | SoundnessViolation s ->
    Format.fprintf fmt "Soundness violation! %s" s

let log_error ?tags err =
  Logging.warn @@ fun _f -> _f
    ~header:(Format.sprintf "%s: Fatal Error" __MODULE__)
    ?tags "@[%a@]" print_error err

exception Error of error * Printexc.raw_backtrace option