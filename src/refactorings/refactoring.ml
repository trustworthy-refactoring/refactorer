open Containers
open   Fun

open Elements.Base
open Identifier

open Fileinfos

open Refactoring_error
open Refactoring_sigs

open Lib

module Core : sig end = struct end

module Repr = Refactoring_repr
module Deps = Refactoring_deps

module Identity : Refactoring = struct
  let name = "Identity"
  let initialise _ = ()
  let process_file _ = Replacement.Set.empty
  let to_repr _ = Repr.Identity
  let get_deps ~mli _ = Deps.Elt.Set.empty
  let kernel_mem _ = true
  let kernel _ = []
end

let of_repr =
  function
  | Repr.Identity ->
    (module Identity : Refactoring)
  | Repr.Rename ((lib, Chain.Ex (Value, from)), _to) ->
    let module R = Rename_val.Make () in
    let () = R.init ((lib, from), _to) in
    (module R : Refactoring)
  | _ ->
    not_implemented ()

let mk_repr (r, params) =
  match r with
  | "rename" ->
    begin match params with
    | [from; _to] ->
      let () = Logging.debug @@ fun _f -> _f
        "Creating representation of \"rename\" with parameters: \"%s\", \"%s\""
        from _to in
      Repr.Rename (of_string from, _to)
    | _ ->
      invalid_arg
        (Format.sprintf
          "Wrong number of arguments for [rename]: %a"
            (List.pp String.pp) params)
    end
  | _ when String.equal r Identity.name ->
    Repr.Identity
  | _ ->
    raise Not_found

let get (r, params) = of_repr (mk_repr (r, params))

let process_file cb (get_deps, process_file) f =
  let () = Logging.info @@ fun _f -> _f "Processing %s" f.filename in
  let src_file = Sourcefile.of_fileinfos f in
  let mli_name = get_mli f.filename in
  let mli = Codebase.find_file mli_name cb in
  let mli =
    if Option.is_some mli then mli
    else Codebase.find_file ~lib:f.library (Filename.basename mli_name) cb in
  let () =
    match mli with
    | None -> ()
    | Some f ->
      Logging.info @@ fun _f -> _f
        "Found interface file: %a" Fileinfos.pp_filename f in
  let mli = Option.map Sourcefile.of_fileinfos mli in
  let deps = get_deps ~mli src_file in
  let results = process_file src_file in
  let () =
    if not (Replacement.Set.is_empty results) then
      Logging.info @@ fun _f -> _f
        ~tags:(Logging.Tags.preamble "Results")
        "%a" (Replacement.Set.pp Replacement.pp) results in
  (deps, results)

let apply_all rs ?filedeps cb =
  let dependencies =
    match filedeps with
    | Some deps ->
      deps
    | None ->
      Codebase.dependency_graph cb in
  let rec apply_all (worklist, processed) ((deps, results) as acc) =
    if Repr.Set.is_empty worklist then
      acc
    else
      let r = Repr.Set.choose worklist in
      let () = Logging.progress_statement ~log:true @@ fun _f -> _f
        "Performing %s " (Repr.to_string r) in
      let module R = (val of_repr r) in
      let worklist = Repr.Set.remove r worklist in
      let processed = Repr.Set.add r processed in
      let fs =
        R.kernel cb
          |> Fun.flip List.fold_left Fileinfos.Set.empty @@ fun fs f
          -> Fileinfos.Set.(add_list (add f fs) (Fileinfos.Graph.pred dependencies f)) in
      let process f (worklist, (deps, results)) =
        let (new_deps, repls) = process_file cb (R.get_deps, R.process_file) f in
        let candidates = Deps.Elt.Set.get_reprs new_deps in
        let candidates = Repr.Set.diff candidates processed in
        let worklist = Repr.Set.union worklist candidates in
        let deps = Deps.Elt.Set.union deps new_deps in
        let results =
          if Replacement.Set.is_empty repls then
            results
          else
            results
              |> Fileinfos.Map.update f
                  (Option.return
                    % (Option.map_or ~default:repls
                        (Replacement.Set.union repls))) in
        let () = Logging.minor_progress () in
        (worklist, (deps, results)) in
      let (worklist, (r_deps, results)) =
        Fileinfos.Set.fold
          process fs (worklist, (Deps.Elt.Set.empty, results)) in
      let deps = Deps.add_all r r_deps deps in
      let () = Logging.major_progress () in
      apply_all (worklist, processed) (deps, results)
  in
  apply_all (rs, Repr.Set.empty) (Deps.empty, Fileinfos.Map.empty)

let apply r ?filedeps cb =
  apply_all (Repr.Set.singleton r) cb ?filedeps