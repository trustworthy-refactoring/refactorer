open Compiler

open Compiler_utils

type t =
  | Not_initialised of string
    (* Refactoring has not properly initialised. *)
  | Bad_sourcefile of string
    (* Source code representation insufficient to requirements. *)
  | CompilerError of Location.error
    (* An error raised by the compiler libraries during the processing of the
       source. *)
  | SoundnessViolation of string
    (* Refactoring unsound. *)
  | KernelError of string
    (* Error when computing the kernel of a refactoring. *)

let print_error fmt = function
  | Not_initialised s ->
    Format.fprintf fmt "Not initialised! %s" s
  | Bad_sourcefile s ->
    Format.fprintf fmt "Bad sourcefile! %s" s
  | CompilerError e ->
    Location.pp_error fmt e
  | SoundnessViolation s ->
    Format.fprintf fmt "Soundness violation! %s" s
  | KernelError s ->
    Format.fprintf fmt "Kernel Error! %s" s

let log_error ?tags err =
  Logging.warn @@ fun _f -> _f
    ~header:(Format.sprintf "%s: Fatal Error" __MODULE__)
    ?tags "@[%a@]" print_error err

exception Error of t * Printexc.raw_backtrace option