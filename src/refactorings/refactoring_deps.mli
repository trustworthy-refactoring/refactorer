(** A module to represent a collection of dependencies between refactorings.
    We have a model where particular refactorings depend upon other refactorings
    being applied in order to be correct themselves. We also keep various
    metadata about these dependencies, including provenance information -
    i.e. what generated a particular dependency. *)

open Containers
open Compiler

open Lib

module type S = sig

  type t
  (** The abstract type of a collection of refactoring dependencies. *)

  type source = Location.t
  (** The source of a dependency. *)

  type descr =
  | ModuleInclude
  | ModuleIsAliased
  | ModuleSubstitutedInSignature
  | SignatureInclude
  | SignatureIsAliased
  | InterfaceImplemented
  | FunctorApplied
  | ModuleIsFunctorArg
  (** The nature of the dependency - why was it generated? *)

  type provenance = source * descr
  (** Provenance of a dependency. *)

  module Repr : Refactoring_repr.S
  (** Runtime representation of refactorings *)

  module Elt : sig

    type t = Repr.t * provenance
    (** The type of a dependency. *)

    module Set : sig
      include SetWithMonoid.S with type elt = t
      val get_reprs : t -> Repr.Set.t
    end
    (** Sets of dependencies *)

    val compare : t -> t -> int

    val equal : t -> t -> bool

    val pp : t Format.printer
    val pp_descr : descr Format.printer

  end
  (** The [Elt] submodule represents refactoring dependencies. *)

  val empty : t
  (** The empty collection of dependencies. *)

  val add : Repr.t -> Elt.t -> t -> t
  (** [add r d deps] adds a dependency of [r] upon [d] arising from to [deps]
      and returns the resulting dependency collection. *)

  val add_all : Repr.t -> Elt.Set.t -> t -> t
  (** [add_all r ds deps] adds a dependency of [r] upon each of the dependencies
      in [ds] to [deps] and returns the resulting dependency collection. *)

  val merge : t -> t -> t
  (** merges two collections of dependencies. *)

  val equal : t -> t -> bool
  (** Tests two collections of dependecies for equality. *)

  val find : Repr.t -> t -> Elt.Set.t
  (** [find r deps] returns a set of all the dependencies of of [r] in [deps]. *)

  val dependents : t -> Repr.Set.t
  (** The set of all dependents, i.e. all refactorings for which dependencies
      are registered. *)

  val pp : t Format.printer

end
(** The signature for Dependency collections, of which this module is the
    canonical implementation. *)

include S with module Repr := Refactoring_repr