open Containers

open Lib

module type S = sig
  type t =
    | Identity
    | Rename of Identifier._t * string
  val pp : t Format.printer
  val to_string : t -> string
  val compare : t -> t -> int
  val equal : t -> t -> bool
  module Set : SetWithMonoid.S with type elt = t
  val identity : t
end

module Orderable = struct
  type t =
    | Identity
    | Rename of Identifier._t * string
  let compare = Stdlib.compare
  (* TODO: remove use of polymorphic compare? *)
  let equal t t' =
    compare t t' = 0
end
include Orderable
module Set = SetWithMonoid.Make(Orderable)
let pp fmt = function
| Identity -> Format.fprintf fmt "@[Identity@]"
| Rename (((_, (Identifier.Chain.Ex (kind, _))) as id), _to) ->
  Format.fprintf fmt "@[Rename %a: %a -> %s@]"
    Elements.Base.pp kind
    Identifier._pp id
    _to
let to_string = mk_to_string pp
let identity = Identity
let rename (id, name) = Rename (id, name)
