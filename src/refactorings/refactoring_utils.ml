open Containers
open   Format

open Compiler

open Refactoring_error

open Sourcefile
open Fileinfos

open Typedtree_views
open Identifier

open Ast_utils
open Compiler_utils

open Buildenv

let lookup_path ~lookup_f ?backup_lib ~env ((_, lid) as id) =
  try
    let path = wrap_lookup lookup_f ?backup_lib id in
    let normalized_path =
      Env.normalize_module_path (Some Location.none) env path in
    if Path.same path normalized_path then
      Some path
    else
      (* TODO: Raise an exception in this case? *)
      let () = Logging.info @@ fun _f -> _f
        "Identifier %a is an alias of %a - will not rename."
        Printtyp.path path
        Printtyp.path normalized_path in
      None
  with
    | Not_found ->
      let backtrace = get_raw_backtrace () in
      let () = Logging.debug @@ fun _f -> _f
        "@[<v 4>could not resolve path to %a: %a@]"
          Printtyp.longident lid
          (some (of_to_string Printexc.raw_backtrace_to_string)) backtrace in
      None
    | (Env.Error e) as err ->
      let backtrace = get_raw_backtrace () in
      let () = Logging.debug @@ fun _f -> _f
        "%a: raised at %a" Env.report_error e
        (some (of_to_string Printexc.raw_backtrace_to_string)) backtrace in
      raise err

module InputState = struct

  module type S = sig
    val get_input : unit -> Sourcefile.t
    val set_input : Sourcefile.t -> unit
    val input_module : unit -> string
    val input_lib : unit -> string option
    val dispatch_on_input_type :
      intf_f:(Typedtree.signature -> 'a) -> impl_f:(Typedtree.structure -> 'a)
        -> 'a
    val find_path_to_local_binding :
      (Ident.t, 'a) Atom.t -> Identifier.Chain._t Option.t
  end

  module Make () = struct

    let input = ref None

    let get_input () = Option.get_exn !input
    let set_input _input =
      input := Some _input
    let input_module () =
      Fileinfos.module_name (get_input ()).fileinfos
    let input_lib () =
      (get_input ()).fileinfos.library
    let dispatch_on_input_type ~intf_f ~impl_f =
      let _exn = Error ((Bad_sourcefile "No typed AST to process!"), None) in
      let intf_f = function
        | (_, Some _sig) -> intf_f _sig
        | _ -> raise _exn in
      let impl_f = function
        | (_, Some _struct) -> impl_f _struct
        | _ -> raise _exn in
      let input = get_input () in
      process_ast ~intf_f ~impl_f input
    let find_path_to_local_binding id =
      let modname = module_name (get_input ()).fileinfos in
      dispatch_on_input_type
        ~intf_f:(fun _sig -> find_path_to_binding id (modname, Sig _sig))
        ~impl_f:(fun _struct -> find_path_to_binding id (modname, Str _struct))


  end

end