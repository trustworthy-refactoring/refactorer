open Containers
open Compiler

open Fun
open Lib

module type S =
sig
  type t
  type source = Location.t
  type descr =
    | ModuleInclude
    | ModuleIsAliased
    | ModuleSubstitutedInSignature
    | SignatureInclude
    | SignatureIsAliased
    | InterfaceImplemented
  type provenance = source * descr
  module Repr : Refactoring_repr.S
  module Elt : sig
    type t = Repr.t * provenance
    module Set : sig
      include SetWithMonoid.S with type elt = t
      val get_reprs : t -> Repr.Set.t
    end
    val compare : t -> t -> int
    val equal : t -> t -> bool
    val pp : t Format.printer
    val pp_descr : descr Format.printer
  end
  val empty : t
  val add : Repr.t -> Elt.t -> t -> t
  val add_all : Repr.t -> Elt.Set.t -> t -> t
  val merge : t -> t -> t
  val equal : t -> t -> bool
  val find : Repr.t -> t -> Elt.Set.t
  val dependents : t -> Repr.Set.t
  val pp : t Format.printer
end

type source = Location.t
type descr =
  | ModuleInclude
  | ModuleIsAliased
  | ModuleSubstitutedInSignature
  | SignatureInclude
  | SignatureIsAliased
  | InterfaceImplemented
type provenance = source * descr

module Repr = Refactoring_repr

module Elt = struct

  module OrderedKernel =
    OrderedPair
      (Repr)
      (OrderedPair
        (struct type t = source let compare = compare end)
        (struct type t = descr  let compare = compare end))

  include OrderedKernel

  let equal d d' =
    compare d d' = 0

  let pp_descr fmt =
    function
    | ModuleInclude ->
      Format.fprintf fmt "ModuleInclude"
    | ModuleIsAliased ->
      Format.fprintf fmt "ModuleIsAliased"
    | ModuleSubstitutedInSignature ->
      Format.fprintf fmt "ModuleSubstitutedInSignature"
    | SignatureInclude ->
      Format.fprintf fmt "SignatureInclude"
    | SignatureIsAliased ->
      Format.fprintf fmt "SignatureIsAliased"
    | InterfaceImplemented ->
      Format.fprintf fmt "InterfaceImplemented"
  let pp = Pair.pp Repr.pp (Pair.pp Location.print_compact pp_descr)

  module Set = struct
    include SetWithMonoid.Make(OrderedKernel)
    let get_reprs ds =
      fold (fun (r, _) rs -> Repr.Set.add r rs) ds Repr.Set.empty
  end

end

module Map = Map.Make(Repr)

type t = Elt.Set.t Map.t

let empty = Map.empty
let equal = Map.equal Elt.Set.equal
let find = Map.find
let dependents = Map.to_list %> (List.map fst) %> Repr.Set.of_list

let add r d =
  Map.update r
    begin function
    | None ->    Some (Elt.Set.singleton d)
    | Some ds -> Some (Elt.Set.add d ds)
    end

let add_all r ds m =
  if Elt.Set.is_empty ds && Map.mem r m then
    m
  else
    m |>
    Map.update r
      begin function
      | None ->     Some ds
      | Some ds' -> Some (Elt.Set.union ds ds')
      end

let merge =
  Map.merge_safe
    ~f:
      begin
        fun r ->
          function
          | `Left ds
          | `Right ds ->       Some ds
          | `Both (ds, ds') -> Some (Elt.Set.union ds ds')
      end

let pp =
  Map.pp ~arrow:" : "
    Repr.pp
    (fun fmt ->
      Format.fprintf fmt "[@,    @[<v>%a@]@,  ]"
        (Elt.Set.pp
          (fun fmt ->
            Format.fprintf fmt "@[<h>%a@]" Elt.pp)))