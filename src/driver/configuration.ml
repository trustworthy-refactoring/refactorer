open Containers

open Compiler

let tool_name = ref "Refactorer"

let implementation_suffix = ref ".ml"

let is_ocaml f =
  let matches = String.equal (Filename.extension f) in
  matches !implementation_suffix
    ||
  matches !Config.interface_suffix
