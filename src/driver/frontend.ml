open Containers
open   IO
open   Format

open Compiler

open Rotor
open   Configuration
open   Buildenv
open   Sourcefile
open   Lib

let include_dirs =
  let docs = Cmdliner.Manpage.s_common_options in
  let doc = "Specify directory containing compiled artifacts." in
  let docv = "DIR" in
  let arg_info = Cmdliner.Arg.info ~docs ~doc ~docv ["I"; "include"] in
  Cmdliner.Arg.(value & opt_all dir [] arg_info)

let init_path paths =
  let () = 
    if not (List.is_empty paths) then 
      Logging.info @@ fun _f -> _f
        "@[<v 4>Adding directories to the include path:@,%a@]"
        (List.pp Format.string_quoted) paths in
  Clflags.include_dirs := paths ;
  init_path ()
  
let combine_paths paths use_merlin =
  let merlin_paths = if use_merlin then Merlinfile.include_dirs () else [] in
  paths @ merlin_paths

let ( & ) t t' =
  Cmdliner.Term.(const (fun () x -> x) $ t $ t')

let with_common_opts t =
  Logging.(Cmdliner.Term.(const set_logging $ cmdline_opts))
    &
  Output.(Cmdliner.Term.(const (Option.iter set) $ output_file))
    &
  Cmdliner.Term.(const init_path $ ((const combine_paths) $ include_dirs $ Merlinfile.use))
    &
  t