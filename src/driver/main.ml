open Containers

open IO
open Fun

open Compiler

open Compmisc
open Config

open Refactoring
open Refactoring_lib
open Lib

open Frontend

open Sourcefile
open Fileinfos

let refactoring : string option ref = ref None
let params = ref []

let rdeps_file : string option ref = ref None

let dependencies = ref (Graph.create ~size:0 ())
(* The initial value gets thrown away later. *)

let set_refactoring r =
  refactoring := Some r

let set_rdep_file f =
  rdeps_file := Some f

let speclist =
  Frontend.speclist @
  Logging.speclist @
  [
    ("-r", Arg.String set_refactoring,
      "<name>: perform refactoring <name>.") ;
    ("-rdeps", Arg.String set_rdep_file,
      "<file>: save refactoring dependency information to <file>.") ;
  ]

let usage = "usage: " ^ (Filename.basename (Sys.executable_name))
  ^ Frontend.usage
  ^ Logging.usage
  ^ " [-rdeps <file>] -r <refactoring> <parameter-list>"

let process_file (get_deps, process_file) f =
  let () = Logging.info @@ fun _f -> _f "Processing %s" f.filename in
  let src_file = Sourcefile.of_fileinfos f in
  let cb = Lazy.force codebase in
  let mli_name = get_mli f.filename in
  let mli = Codebase.find_file mli_name cb in
  let mli =
    if Option.is_some mli then mli
    else Codebase.find_file ~lib:f.library (Filename.basename mli_name) cb in
  let () =
    match mli with
    | None -> ()
    | Some f ->
      Logging.info @@ fun _f -> _f
        "Found interface file: %a" Fileinfos.pp_filename f in
  let mli = Option.map Sourcefile.of_fileinfos mli in
  let deps = get_deps ~mli src_file in
  let results =
    (* TODO: we don't really want to swallow errors here *)
    try process_file src_file
    with Error (err, backtrace) ->
      let () =
        log_error
          ~tags:(Logging.Tags.preamble "Error applying refactoring")
          err in
      let () =
        backtrace
          |> Option.map_or ~default:() @@ fun backtrace
          -> Logging.debug @@ fun _f -> _f
              "%s" (Printexc.raw_backtrace_to_string backtrace) in
      Replacement.Set.empty in
  (deps, results)

let apply_with_all_deps rs =
  let cb = Lazy.force codebase in
  let dependencies = Lazy.force file_dependencies in
  let rec apply_with_all_deps (worklist, processed) ((deps, results) as acc) =
    if Repr.Set.is_empty worklist then
      acc
    else
      let r = Repr.Set.choose worklist in
      let () = Logging.progress_statement ~log:true @@ fun _f -> _f
        "Performing %s " (Repr.to_string r) in
      let module R = (val Refactoring_lib.of_repr r) in
      let worklist = Repr.Set.remove r worklist in
      let processed = Repr.Set.add r processed in
      let fs =
        R.kernel cb
          |> Fun.flip List.fold_left Fileinfos.Set.empty @@ fun fs f
          -> Fileinfos.Set.(add_list (add f fs) (Graph.pred dependencies f)) in
      let process f (worklist, (deps, results)) =
        let (new_deps, repls) = process_file (R.get_deps, R.process_file) f in
        let candidates = Deps.Elt.Set.get_reprs new_deps in
        let candidates = Repr.Set.diff candidates processed in
        let worklist = Repr.Set.union worklist candidates in
        let deps = Deps.Elt.Set.union deps new_deps in
        let results =
          if Replacement.Set.is_empty repls then
            results
          else
            results
              |> Fileinfos.Map.update f
                  (Option.return
                    % (Option.map_or ~default:repls
                        (Replacement.Set.union repls))) in
        let () = Logging.minor_progress () in
        (worklist, (deps, results)) in
      let (worklist, (r_deps, results)) =
        Fileinfos.Set.fold
          process fs (worklist, (Deps.Elt.Set.empty, results)) in
      let deps = Deps.add_all r r_deps deps in
      let () = Logging.major_progress () in
      apply_with_all_deps (worklist, processed) (deps, results)
  in
  apply_with_all_deps (rs, Repr.Set.empty) (Deps.empty, Fileinfos.Map.empty)

let () = Arg.parse speclist (fun arg -> params := arg :: !params) usage

let () =
  Logs.set_reporter (Logging.reporter ()) ;
  Logs.set_level !Logging.log_level

let () = init_path false

let () =
  match !refactoring with
  | None ->
    begin
      prerr_endline ("A refactoring must be specified!") ;
      prerr_endline usage ;
      exit 1 ;
    end
  | Some _ -> ()

let () =
  let r =
    !refactoring
      |> Option.map_or ~default:(module Identity : S) @@ fun r
      -> try
           get (r, List.rev !params)
         with Not_found ->
           failwith (Format.sprintf "%s: unknown refactoring" r) in
  let module R = (val r) in
  let codebase = Lazy.force codebase in
  let () = Logging.major_progress ~msgf:(fun _f -> _f
    "Codebase contains %i files" (Codebase.cardinal codebase)) () in
  let (deps, results) =
    apply_with_all_deps (Repr.Set.singleton (R.to_repr ())) in
  let () = Logging.info @@ fun _f -> _f
    ~header:"Refactoring Set"
    "@[<v>%a@]" (Repr.Set.pp Repr.pp) (Deps.dependents deps) in
  let () =
    match !rdeps_file with
    | None -> ()
    | Some f ->
      with_out f @@ fun f ->
        Format.fprintf (Format.formatter_of_out_channel f)
          "@[<v>%a@]@." Deps.pp deps in
  let () =
    Logging.major_progress ~msgf:(fun _f -> _f
      "Requires changes to %i files" (Fileinfos.Map.cardinal results)) () in
  results |> Fileinfos.Map.iter @@ fun f results ->
    let src_file = Sourcefile.of_fileinfos f in
    let () =
      Logging.debug @@ fun _f -> _f
        "Performing the following replacements in %s:@,@[%a@]"
        f.filename (Replacement.Set.pp Replacement.pp) results in
    let results = Replacement.apply_all results src_file.contents in
    let results = Sourcefile.diff src_file results in
    let () =
      Logging.info @@ fun _f -> _f
        "Computed patch for %s" f.filename in
    if String.length results <> 0 then
      print_endline results
