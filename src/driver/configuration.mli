val tool_name : string ref

val implementation_suffix : string ref
(** The suffix for implementation files (i.e. ".ml"). *)

val is_ocaml : string -> bool
(** [is_ocaml f] returns true if and only if [f] is a valid OCaml file
    (determined using file extensions). *)