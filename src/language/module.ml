open Containers
open Compiler

open Identifier

open Typedtree

type name_t = string

let rec unwrap_expr acc me =
  match me.mod_desc with
  | Tmod_constraint (me, _, Tmodtype_implicit, _) ->
      unwrap_expr acc me
  | Tmod_constraint (me, _, Tmodtype_explicit ty, _) ->
      unwrap_expr (ty :: acc) me
  | _ -> me, (List.rev acc)

let unwrap_expr me = unwrap_expr [] me

let dest_ident_expr me =
  let me, frames = unwrap_expr me in
  match me.mod_desc with
  | Tmod_ident (path, id) -> Some (frames, (path, id))
  | _ -> None

let check_atm : type a . (_, a) Atom.t -> _ =
  function
  | Atom.Structure _ as a ->
    Some (Atom.Ex a)
  | Atom.Functor _ as a ->
    Some (Atom.Ex a)
  | _ ->
    None

let check_ex_atm (Atom.Ex a) = check_atm a

module Binding = struct
  let impl_sort mb =
    Option.map
      Elements.ModuleType.type_of
      (Moduletype.sort mb.mb_expr.mod_env mb.mb_expr.mod_type)
  let intf_sort md =
    Option.map
      Elements.ModuleType.type_of
      (Moduletype.sort md.md_type.mty_env md.md_type.mty_type)
end