(** Functions for manipulating module elements in the AST *)

open Containers
open Compiler

type name_t = string

val dest_ident_expr : Typedtree.module_expr ->
  (Typedtree.module_type list * (Path.t * Longident.t Asttypes.loc)) option

val unwrap_expr : Typedtree.module_expr ->
  Typedtree.module_expr * Typedtree.module_type list
(** [unwrap_expr me] strips the module expression [me] of its module type
    constraints, and returns them along with the inner module expression. *)

val unwind_expr :
  Typedtree.module_expr
    -> Typedtree.module_expr
        * (Typedtree.module_expr * Typedtree.module_type list) list
        * Typedtree.module_type list
(** [unwind_expr me] first strips the module expression [me] of its module type
    constraints, and then collects any module expressions that are used as
    arguments along with any module type constraints of the module expression
    that each is applied to, and then returns these with the inner module
    expression. For example, if [me] is of the following form:

      (( ... ((me' : mtys_1) (me_1))  ... : [mtys_n]) ([me_n])) : [mtys_0]

    then [unwind_expr me] returns:

      (me', [(me_1, mtys_1); ...; (me_n, mtys_n)], mtys_0).
*)

val check_atm : ('a, 'b) Identifier.Atom.t -> 'a Identifier.Atom._t Option.t
val check_ex_atm : 'a Identifier.Atom._t -> 'a Identifier.Atom._t Option.t

module Binding : sig
  val impl_sort : Typedtree.module_binding -> Elements.Module._sub_t Option.t
  val intf_sort : Typedtree.module_declaration -> Elements.Module._sub_t Option.t
end