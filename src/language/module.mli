(** Functions for manipulating module elements in the AST *)

open Containers
open Compiler

type name_t = string

val dest_ident_expr : Typedtree.module_expr ->
  (Typedtree.module_type list * (Path.t * Longident.t Asttypes.loc)) option

val unwrap_expr : Typedtree.module_expr ->
  Typedtree.module_expr * Typedtree.module_type list

val check_atm : ('a, 'b) Identifier.Atom.t -> 'a Identifier.Atom._t Option.t
val check_ex_atm : 'a Identifier.Atom._t -> 'a Identifier.Atom._t Option.t

module Binding : sig
  val impl_sort : Typedtree.module_binding -> Elements.Module._sub_t Option.t
  val intf_sort : Typedtree.module_declaration -> Elements.Module._sub_t Option.t
end