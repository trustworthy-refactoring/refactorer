open Containers
open Fun

open MParser
open MParser_RE

open Compiler

open Lib

open Elements
open Elements.Base

(* Regular expressions for types of identifier - taken from section 6.1 of the
   OCaml Language Specification. *)
let ident = make_regexp "[_a-zA-Z][_0-9a-zA-Z']*"
let capitalized_ident = make_regexp "[A-Z][_0-9a-zA-Z']*"
let lowercase_ident = make_regexp "[_a-z][_0-9a-zA-Z']*"

let infix_symbol =
  make_regexp
    "([=<>@\\^|&+\\-*/$%][!$%&*+\\-./:<=>?@\\^|~]*)|#[!$%&*+\\-./:<=>?@\\^|~]+"

let prefix_symbol =
  make_regexp
    "(![!$%&*+\\-./:<=>?@\\^|~]*)|[?~][!$%&*+\\-./:<=>?@\\^|~]+"

let matches rex s =
  Option.is_some (Regexp.exec ~rex ~pos:0 (Bytes.of_string s))

let is_ident = matches ident
let is_capitalized_ident = matches capitalized_ident
let is_lowercase_ident = matches lowercase_ident

let parse_ident = regexp ident
let parse_capitalized_ident = regexp capitalized_ident
let parse_lowercase_ident = regexp lowercase_ident
let parse_infix_symbol = regexp infix_symbol
let parse_prefix_symbol = regexp prefix_symbol

(* Parsers for names - from section 6.3 of the OCaml Language Specification. *)
let parse_value_name =
  parse_lowercase_ident
  <|> between (char '(') (char ')')
        (parse_infix_symbol <|> parse_prefix_symbol)
let parse_constructor_name = parse_capitalized_ident
let parse_typeconstructor_name = parse_lowercase_ident
let parse_field_name = parse_lowercase_ident
let parse_module_name = parse_capitalized_ident
let parse_moduletype_name = parse_ident
let parse_class_name = parse_lowercase_ident
let parse_inst_var_name = parse_lowercase_ident
let parse_method_name = parse_lowercase_ident

(* Prefix character signifiers for elements *)

let value_signifier = ':'
let structure_signifier = '.'
let structure_type_signifier  = '%'
let functor_signifier = '#'
let functor_type_signifier = '*'

let pp_signifier (type a) : a Base.t Format.printer =
  fun fmt kind ->
    let signifier =
      match kind with
      | Value -> value_signifier
      | Structure -> structure_signifier
      | StructureType -> structure_type_signifier
      | Functor -> functor_signifier
      | FunctorType -> functor_type_signifier
      | Parameter ->
        invalid_arg (Format.sprintf "%s.pp_signifier" __MODULE__) in
      Format.char fmt signifier

(* Parsers of prefix signifier characters for parsing chain links *)
let parse_value_signifier = char value_signifier
let parse_structure_signifier = char structure_signifier
let parse_structuretype_signifier = char structure_type_signifier
let parse_functor_signifier = char functor_signifier
let parse_functor_type_signifier = char functor_type_signifier

type 'a param_info =
  | Indexed of int
  | Named of 'a

let map_param f =
  function
  | Indexed idx -> Indexed idx
  | Named id -> Named (f id)

let pp_param pp fmt =
  function
  | Indexed idx ->
    Format.fprintf fmt "[%i]" idx
  | Named id ->
    Format.fprintf fmt "[%a]" pp id

let parse_param st =
  let open MParser_RE.Tokens in
    (squares (
          (attempt decimal |>> (fun idx ->
            Indexed idx))
      <|> (parse_lowercase_ident |>> (fun id ->
            Named id)))
     ) st

module Atom = struct

  module Data = struct

    type 'a t = {
      id  : 'a ;
      idx : int Option.t ;
    }

    let equal f d d' =
      f d.id d'.id
        &&
      Option.equal Int.equal d.idx d'.idx

    let pp _pp fmt d =
      match d.idx with
      | None ->
        _pp fmt d.id
      | Some idx ->
        Format.fprintf fmt "[%i]" idx

    let only id = { id; idx = None; }

  end

  type ('payload, 'kind) t =
    | Value         : 'a                -> ('a, _value)          t
    | Parameter     : 'a * int Option.t -> ('a, _parameter)      t
    | Structure     : 'a                -> ('a, _structure)      t
    | StructureType : 'a                -> ('a, _structure_type) t
    | Functor       : 'a                -> ('a, _functor)        t
    | FunctorType   : 'a                -> ('a, _functor_type)   t

  (* exception UnsupportedElement of Elements._t *)

  exception IncompatiblePayload

  let mk (type kind) : kind Base.t -> 'a Data.t -> ('b, kind) t =
    let open Data in
    fun kind payload ->
      match kind with
      | Value -> Value payload.id
      | Parameter ->
        begin match payload.idx with
        | Some idx when idx < 1 ->
          invalid_arg
            (Format.sprintf "%s.Atom.mk: invalid parameter index" __MODULE__)
        | _ ->
          Parameter (payload.id, payload.idx)
        end
      | Structure -> Structure payload.id
      | StructureType -> StructureType payload.id
      | Functor -> Functor payload.id
      | FunctorType -> FunctorType payload.id

  type 'payload _t = Ex : ('a, _) t -> 'a _t

  let _mk kind d =
    let Base.Ex kind = kind in
    Ex (mk kind d)

  (* Merlin gives me an error if I do not put the type annotation in, however
     the compiler seems to be happy. *)
  let dest : type a b . (a, b) t -> a Data.t * b Base.t =
    let open Data in
    function
    | Value id -> only id, Value
    | Parameter (id, idx) -> { id; idx; }, Parameter
    | Structure id -> only id, Structure
    | StructureType id -> only id, StructureType
    | Functor id -> only id, Functor
    | FunctorType id -> only id, FunctorType

  let unwrap a = fst (dest a)
  let kind a = snd (dest a)

  let equal eq (type a b) (a : ('c, a) t) (b : ('c, b) t) =
    let (d, kind) = dest a in
    let (d', kind') = dest b in
    Elements.Base.equal kind kind'
      &&
    Data.equal eq d d'

  let map : type c . ('a -> 'b) -> ('a, c) t -> ('b, c) t =
    fun f ->
      function
      | Value x -> Value (f x)
      | Parameter (id, idx) -> Parameter (f id, idx)
      | Structure x -> Structure (f x)
      | StructureType x -> StructureType (f x)
      | Functor x -> Functor (f x)
      | FunctorType x -> FunctorType (f x)

  let pp : type b . ('a Format.printer) -> ('a, b) t Format.printer =
    fun pp fmt a ->
      Format.pair (Data.pp pp) Base.pp fmt (dest a)

  let _pp _pp fmt (Ex atm) = pp _pp fmt atm

  (* include Wrappers.Module.Make1 (struct type ('a, 'b) data = ('a, 'b) t end)
  include Wrappers.ModuleType.Make1 (struct type ('a, 'b) data = ('a, 'b) t end)

  let lift_module : ('a, _module) t -> 'a module_t =
    function
    | Module (S, a) -> S (Structure a)
    | Module (F, a) -> F (Functor a)

  let lift_module_type : ('a, _module_type) t -> 'a module_type_t =
    function
    | ModuleType (ST, a) -> ST (StructureType a)
    | ModuleType (FT, a) -> FT (FunctorType a) *)

end

module Chain = struct

  (* TODO: Lift constraints on building chains to the type level as follows *)
  (*
  type ('hook, 'latch) link_builder =
    | M_V : (_module, _value) link_builder
      (* A module can have a value child *)
    | M_M : (_module, _module) link_builder
      (* A module can have a module child *)
    | M_MT : (_module, _module_type) link_builder
      (* A module can have a module type child *)
    | MT_V : (_module_type, _value) link_builder
      (* A module type can have a value child *)
    | MT_M : (_module_type, _module) link_builder
      (* A module type can have a module child *)
    | MT_MT : (_module_type, _module_type) link_builder
      (* A module type can have a module type child *)

  type ('latch, 'anchor) chain = private
    | Atomic
        : (string, 'a) Atom.t -> ('a, 'a) chain
    | InValue
        : string * (_value, 'a) primed_chain -> (_value, 'a) chain
    | InModule
        : string * (_module, 'a) primed_chain -> (_module, 'a) chain
    | InModuleType
        : string * (_module_type, 'a) primed_chain -> (_module_type, 'a) chain

  and ('socket, 'anchor) primed_chain =
    Hook : ('a, 'b) link_builder * ('b, 'c) chain -> ('a, 'c) primed_chain

  type _t =
    (* N.B. Valid top-level identifiers always latch to a module *)
    | Ex : 'a Elements.t * (_module, 'a) t
  *)

  type 'a t =
    | Atomic          : (string, 'a) Atom.t      -> 'a t
    | InStructure     : string * 'a t            -> 'a t
    | InStructureType : string * 'a t            -> 'a t
    | InFunctor       : string * 'a t            -> 'a t
    | InFunctorType   : string * 'a t            -> 'a t
    | InParameter     : string param_info * 'a t -> 'a t
    (* This data type needn't be a GADT, but we have made it so in preparation
       for representing functor applications, as follows. *)
    (* TODO: Incorporate functor applications - the type should be [_module chain] *)
    (*| FunctorApp : _functor t * _structure t -> _module t *)

  type _t =
    | Ex : 'a Base.t * 'a t -> _t

  type variety =
    | InIntf
    | InImpl

  let mk a = Atomic a
  let _mk (Atom.Ex a) = Ex (Atom.kind a, Atomic a)

  let cons (type a) (a : (_, a) Atom.t) c =
    let open Atom in
    (* N.B. When the collection of elements is expanded, we may need to also
            match on the head of [c] to make sure that the cons is valid. *)
    match a with
    | Value v ->
      invalid_arg (Format.sprintf "%s.Chain.cons" __MODULE__)
    | Parameter (id, None) ->
      InParameter (Named id, c)
    | Parameter (_, Some idx) ->
      InParameter (Indexed idx, c)
    | Structure m ->
      InStructure (m, c)
    | StructureType t ->
      InStructureType (t, c)
    | Functor m ->
      InFunctor(m, c)
    | FunctorType t ->
      InFunctorType (t, c)

  let hd (type a) : a t -> _ =
    function
    | Atomic a ->
      Atom.Ex a
    | InStructure (m, _) ->
      Atom.Ex (Atom.Structure m)
    | InStructureType (t, _) ->
      Atom.Ex (Atom.StructureType t)
    | InFunctor (m, _) ->
      Atom.Ex (Atom.Functor m)
    | InFunctorType (ft, _) ->
      Atom.Ex (Atom.FunctorType ft)
    | InParameter (Named id, _) ->
      Atom.Ex (Atom.Parameter (id, None))
    | InParameter (Indexed idx, _) ->
      Atom.Ex (Atom.Parameter ("", Some idx))

  let tl (type a) : a t -> a t option =
    function
    | Atomic _
    -> None
    | InStructure (_, tl)
    | InStructureType (_, tl)
    | InFunctor (_, tl)
    | InFunctorType (_, tl)
    | InParameter (_, tl)
    -> Some tl

  let tl_exn (type a) : a t -> a t =
    function
    | Atomic _
    -> invalid_arg (Format.sprintf "%s.Chain.tl_exn" __MODULE__)
    | InStructure (_, tl)
    | InStructureType (_, tl)
    | InFunctor (_, tl)
    | InFunctorType (_, tl)
    | InParameter (_, tl)
    -> tl

  let rec append pre post =
    match pre with
    | Atomic a ->
      cons a post
    | InStructure (s, tl) ->
      InStructure (s, append tl post)
    | InStructureType (s, tl) ->
      InStructureType (s, append tl post)
    | InFunctor (s, tl) ->
      InFunctor (s, append tl post)
    | InFunctorType (s, tl) ->
      InFunctorType (s, append tl post)
    | InParameter (p, tl) ->
      InParameter (p, append tl post)

  let _append c (Ex (sort, c')) = Ex (sort, append c c')

  let rec variety =
    function
    | Atomic _
    | InParameter _
      -> InImpl
    | InStructureType (_, _)
    | InFunctorType (_, _)
      -> InIntf
    | InStructure (_, c)
    | InFunctor (_, c)
      -> variety c

  (* TODO: If we implement this datatype as a finger tree we can get these to be
          O(1) amortized cost. *)
  (* But probably these operations will not be run on identifiers often enough
     for this to make a difference - so we need a different data structure for
     efficiency. *)

  let rec anchor =
    function
    | Atomic a
    -> a
    | InStructure (_, c)
    | InStructureType (_, c)
    | InFunctor (_, c)
    | InFunctorType (_, c)
    | InParameter (_, c)
    -> anchor c

  let lift c =
    Ex (Atom.kind (anchor c), c)

  let rec equal : 'a . 'a t -> 'a t -> _ =
    fun (type a) (c : a t) (c' : a t) ->
      let Atom.Ex hd, Atom.Ex hd' = hd c, hd c' in
      Atom.equal String.equal hd hd'
        &&
      match tl c, tl c' with
      | None, None ->
        true
      | Some tl, Some tl' ->
        equal tl tl'
      | _ ->
        false

  let to_longident c =
    let rec to_longident : type a . Longident.t option -> a t -> Longident.t =
      fun acc ->
        let cons s =
          acc |>
            Option.map_or ~default:(Longident.Lident s)
              (fun id -> Longident.Ldot (id, s)) in
        function
        | Atomic Atom.Value id         -> cons id
        | Atomic Atom.Structure id     -> cons id
        | Atomic Atom.StructureType id -> cons id
        | Atomic Atom.Functor id       -> cons id
        | Atomic Atom.FunctorType id   -> cons id
        | Atomic Atom.Parameter _ ->
          invalid_arg (Format.sprintf "%s.Chain.to_longindent" __MODULE__)
        | InStructure (s, c)
        | InStructureType (s, c)
        | InFunctor (s, c)
        | InFunctorType (s, c)
        -> to_longident (Some (cons s)) c
        | InParameter _ ->
          invalid_arg (Format.sprintf "%s.Chain.to_longindent" __MODULE__) in
    to_longident None c

  let rec pp : type a . a t Format.printer =
    fun fmt ->
      function
      | Atomic Atom.Parameter (id, None) ->
        pp_param Format.string fmt (Named id)
      | Atomic Atom.Parameter (_, Some idx) ->
        pp_param Format.string fmt (Indexed idx)
      | Atomic a ->
        let Atom.Data.{ id; _}, kind = Atom.dest a in
        let id =
          match kind with
          | Value when not (is_lowercase_ident id) ->
            Format.sprintf "(%s)" id
          | _ ->
            id in
        Format.fprintf fmt "%a%s" pp_signifier kind id
      | InStructure (m, c) ->
        Format.fprintf fmt "%c%s%a" structure_signifier m pp c
      | InStructureType (mt, c) ->
        Format.fprintf fmt "%c%s%a" structure_type_signifier mt pp c
      | InFunctor (m, c) ->
        Format.fprintf fmt "%c%s%a" functor_signifier m pp c
      | InFunctorType (ft, c) ->
        Format.fprintf fmt "%c%s%a" functor_type_signifier ft pp c
      | InParameter (p, c) ->
        Format.fprintf fmt "%a%a" (pp_param Format.string) p pp c

  let _pp fmt (Ex (_, c)) = pp fmt c

  let _cons (Atom.Ex link) c =
    cons link c

  let build_from_anchor a links =
    List.fold_left (flip _cons) (Atomic a) links

  let _cons a (Ex (sort, c)) = Ex (sort, cons a c)

  let build =
    function
    | [] -> invalid_arg (Format.sprintf "%s.Chain.build" __MODULE__)
    | (Atom.Ex a) :: xs ->
      let chain = build_from_anchor a xs in
      let kind = snd (Atom.dest a) in
      Ex (kind, chain)

  let try_open (type a) : a Base.t -> _t -> a t =
    fun kind (Ex (kind', c)) ->
      match kind, kind' with
      | Value, Value -> c
      | Structure, Structure -> c
      | StructureType, StructureType -> c
      | Functor, Functor -> c
      | FunctorType, FunctorType -> c
      | Parameter, Parameter -> c
      | _ ->
        raise (KindMismatch (Format.sprintf "not a %a chain!" Base.pp kind))

  let parse_link parent =
    match parent with
    | Some Atom.Ex Atom.Value _ ->
      fail "Expected EOF"
    | _ ->
        (attempt parse_value_signifier >>
          parse_value_name <?> "Value Identifier" |>> (fun
          v -> Atom.Ex (Atom.Value v)))
      <|>
        (attempt parse_structure_signifier >>
          parse_module_name <?> "Module Identifier" |>> (fun
          m -> Atom.Ex (Atom.Structure m)))
      <|>
        (attempt parse_structuretype_signifier >>
          parse_moduletype_name <?> "Module Type Identifier" |>> (fun
          mt -> Atom.Ex (Atom.StructureType mt)))
      <|>
        (attempt parse_functor_signifier >>
          parse_module_name <?> "Module Identifier" |>> (fun
          m -> Atom.Ex (Atom.Functor m)))
      <|>
        (attempt parse_functor_type_signifier >>
          parse_moduletype_name <?> "Module Type Identifier" |>> (fun
          mt -> Atom.Ex (Atom.FunctorType mt)))
      <|>
        (attempt parse_param >>= (fun
          p ->
            let p =
              match p with
              | Named id -> Atom.Parameter (id, None)
              | Indexed idx -> Atom.Parameter ("", Some idx) in
            let p = Atom.Ex p in
            parent |> (
            (function
              | Some Atom.Ex Atom.Functor _ ->
                return p
              | Some Atom.Ex Atom.FunctorType _ ->
                return p
              | _ ->
                fail "Not expecting a parameter!")
                  : string Atom._t option
                      -> (string Atom._t, unit) MParser.parser)))
      <|>
        fail "Expecting Identifier kind signifier!"

  let parse =
    let rec parse links =
      match links with
      | [] ->
        parse_link None <?> "Identifier"
          >>= (fun link -> parse [link])
      | x :: _ ->
        (attempt eof
          >>$ build links)
            <|>
        (parse_link (Some x)
          >>= (fun link -> parse (link::links)))
      in
    parse []

  let of_string = mk_of_string parse

  let of_longident (type a) (k : a Elements.Base.t) p =
    let rec collapse flag acc lid =
      let k =
        match acc with
        | [] ->
          Elements.Base.Ex k
        | _ ->
          if flag then Elements.Base.(Ex Functor)
                  else Elements.Base.(Ex Structure) in
      match lid with
      | Longident.Lident name ->
        Atom.(_mk k (Data.only name)) :: acc
      | Longident.Ldot (lid, id) ->
        collapse false ((Atom.(_mk k (Data.only id))) :: acc) lid
      | Longident.Lapply (lid, _) ->
        collapse true acc lid in
    match k with
    | Elements.Base.Parameter ->
      invalid_arg
        (Format.sprintf
          "%s.of_path: cannot create parameter identifier of path!" __MODULE__)
    | _ ->
      try_open k (build (List.rev (collapse false [] p)))

  let _of_longident (Base.Ex kind) lid =
    Ex (kind, of_longident kind lid)

  let of_path (type a) (k : a Elements.Base.t) p =
    let rec collapse flag acc p =
      let k =
        match acc with
        | [] ->
          Elements.Base.Ex k
        | _ ->
          if flag then Elements.Base.(Ex Functor)
                  else Elements.Base.(Ex Structure) in
      match p with
      | Path.Pident id ->
        Atom.(_mk k (Data.only (Ident.name id))) :: acc
      | Path.Pdot _ ->
        let p, id = Path.dest_dot p in
        collapse false ((Atom.(_mk k (Data.only id))) :: acc) p
      | Path.Papply (p, _) ->
        collapse true acc p in
    match k with
    | Elements.Base.Parameter ->
      invalid_arg
        (Format.sprintf
          "%s.of_path: cannot create parameter identifier of path!" __MODULE__)
    | _ ->
      try_open k (build (List.rev (collapse false [] p)))

  let _of_path (Base.Ex kind) p =
    Ex (kind, of_path kind p)

  include ModuleType.Make_T0 (struct type 'a data = 'a t end)

  let split_at_interface c =
    let rec split_at_interface c acc =
      let (Atom.Ex hd) as _hd, tl = hd c, tl c in
      match hd, tl with
      | Atom.StructureType a, _ ->
        ST (build_from_anchor (Atom.StructureType a) acc), tl
      | Atom.FunctorType a, _ ->
        FT (build_from_anchor (Atom.FunctorType a) acc), tl
      | Atom.Parameter _, _ ->
        invalid_arg
          (Format.sprintf
            "%s.Chain.split_at_interface: encountered parameter link!"
            __MODULE__)
      | _, Some tl ->
        split_at_interface tl (_hd :: acc)
      | _ ->
        invalid_arg
          (Format.sprintf
            "%s.Chain.split_at_interface: no interface link!"
            __MODULE__)
      in
    split_at_interface c []

  let _split_at_interface c =
    match split_at_interface c with
    | ST mty, tail -> Ex (StructureType, mty), tail
    | FT mty, tail -> Ex (FunctorType, mty), tail

  let split_at_functor c =
    let rec split_at_functor c acc =
      let (Atom.Ex hd) as _hd, tl = hd c, tl c in
      match hd, tl with
      | Atom.Functor _, _ ->
        ((build_from_anchor hd acc) : _functor t), tl
      | Atom.Structure _, Some tl ->
        split_at_functor tl (_hd :: acc)
      | _, Some _ ->
        invalid_arg
          (Format.sprintf "%s.Chain.split_at_functor: encountered %a link"
            __MODULE__
            (Atom.pp Format.string) hd)
      | _, None ->
        invalid_arg
          (Format.sprintf
            "%s.Chain.split_at_functor: no functor link!"
            __MODULE__) in
    split_at_functor c []

  let rec in_functor : type a . a t -> bool =
    fun c ->
      let open Atom in
      let Ex hd, tl = hd c, tl c in
      match hd, tl with
      | Functor _, _ ->
        true
      | StructureType _, _ ->
        false
      | FunctorType _, _ ->
        false
      | _, None ->
        false
      | _, Some tl ->
        in_functor tl

  let rec drop : type a b . a t -> b t -> b t option =
    fun c c' ->
      let Atom.Ex a = hd c in
      let Atom.Ex a' = hd c' in
      if Atom.equal String.equal a a' then
        match tl c, tl c' with
        | Some tl, None ->
          invalid_arg
            (Format.sprintf "%s.Chain.drop" __MODULE__)
        | None, tl ->
          tl
        | Some tl, Some tl' ->
          drop tl tl'
      else
        invalid_arg
          (Format.sprintf "%s.Chain.drop" __MODULE__)

  let promote ?(by=1) (type a) (c : a t) =
    if by < 1 then
      invalid_arg (Format.sprintf "%s.Chain.promote" __MODULE__) ;
    match c with
    | InParameter (Indexed idx, tl) ->
      InParameter (Indexed (idx + by), tl)
    | _ ->
      c

  let demote ?(by=1) (type a) (c : a t) =
    if by < 1 then
      invalid_arg (Format.sprintf "%s.Chain.demote: ?by < 1" __MODULE__) ;
    match c with
    | InParameter (Indexed idx, tl) when idx - by >= 0 ->
      InParameter (Indexed (idx - by), tl)
    | _ ->
      invalid_arg (Format.sprintf "%s.Chain.demote" __MODULE__)

  end

type 'a t = string option * 'a Chain.t

type _t = string option * Chain._t

let pp fmt (lib, c) =
  Format.fprintf fmt "%a%a"
    (Format.some Format.string) lib
    Chain.pp c

let _pp fmt (lib, c) =
  Format.fprintf fmt "%a%a"
    (Format.some Format.string) lib
    Chain._pp c

let parse =
  option parse_ident >>= (fun lib ->
    Chain.parse |>> (fun c ->
    lib, c))

let of_string = mk_of_string parse

let lib (lib, _) = lib
let chain (_, c) = c

let equal (lib, c) (lib', c') =
  Option.equal String.equal lib lib'
    &&
  Chain.equal c c'