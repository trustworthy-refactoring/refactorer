open Compiler

open Containers

open Elements
open Elements.Base

type 'a param_info =
  | Indexed of int
  | Named of 'a

module Atom : sig

  module Data : sig

    type 'a t = {
      id  : 'a ;
      idx : int Option.t ;
    }

    val only : 'a -> 'a t

    val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
    val pp : 'a Format.printer -> 'a t Format.printer

  end

  type ('payload, 'kind) t = private
    | Value         : 'a                -> ('a, _value)          t
    | Parameter     : 'a * int Option.t -> ('a, _parameter)      t
    | Structure     : 'a                -> ('a, _structure)      t
    | StructureType : 'a                -> ('a, _structure_type) t
    | Functor       : 'a                -> ('a, _functor)        t
    | FunctorType   : 'a                -> ('a, _functor_type)   t

  type 'payload _t = Ex : ('a, _) t -> 'a _t

  val mk : 'b Base.t -> 'a Data.t -> ('a, 'b) t

  val _mk : Base._t -> 'a Data.t -> 'a _t

  val dest : ('a, 'b) t -> 'a Data.t * 'b Base.t
  (** [dest a] returns the payload of the atom [a] and an [Elements.t] value
      describing the phantom type parameter of [a]. *)
  val unwrap : ('a, _) t -> 'a Data.t
  (** [unwrap a] is [fst (dest a)]. *)
  val kind : (_, 'b) t -> 'b Base.t
  (** [kind a] is [snd (dest a)]. *)

  val map : ('a -> 'b) -> ('a, 'c) t -> ('b, 'c) t

  val pp : 'a Format.printer -> ('a, 'b) t Format.printer
  val _pp : 'a Format.printer -> 'a _t Format.printer

  val equal : ('c -> 'c -> bool) -> ('c, 'a) t -> ('c, 'b) t -> bool

  (* include Wrappers.Module.S1     with type ('a, 'b) data := ('a, 'b) t
  include Wrappers.ModuleType.S1 with type ('a, 'b) data := ('a, 'b) t *)

  exception IncompatiblePayload

end

module Chain : sig

  type 'a t = private
    | Atomic          : (string, 'a) Atom.t      -> 'a t
    | InStructure     : string * 'a t            -> 'a t
    | InStructureType : string * 'a t            -> 'a t
    | InFunctor       : string * 'a t            -> 'a t
    | InFunctorType   : string * 'a t            -> 'a t
    | InParameter     : string param_info * 'a t -> 'a t

  type _t =
    | Ex : 'b Base.t * 'b t -> _t

  type variety =
    | InIntf
    | InImpl
  (** The "variety" of a chain: whether it refers to an element in:
        - an interface (i.e. an explicitly named module type); or
        - an implementation (i.e. a structure). *)

  include ModuleType.T0 with type 'a data := 'a t

  (* CONSTUCTORS *)

  val mk : (string, 'a) Atom.t -> 'a t
  val _mk : string Atom._t -> _t
  val cons : (string, 'a) Atom.t -> 'b t -> 'b t
  val _cons : (string, 'a) Atom.t -> _t -> _t
  val append : 'a t -> 'b t -> 'b t
  val _append : 'a t -> _t -> _t
  val lift : 'a t -> _t
  val build : string Atom._t list -> _t

  val of_string : string ->  _t
  val of_longident : 'b Base.t -> Longident.t -> 'b t
  val _of_longident : Base._t -> Longident.t -> _t
  val of_path : 'b Base.t -> Path.t -> 'b t
  val _of_path : Base._t -> Path.t -> _t

  (* DESTRUCTORS *)

  val hd : 'a t -> string Atom._t
  val tl : 'a t -> 'a t option
  val tl_exn : 'a t -> 'a t

  val anchor : 'a t -> (string, 'a) Atom.t

  val try_open : 'b Base.t -> _t -> 'b t

  val split_at_interface : 'a t -> module_type_t * 'a t option
  val _split_at_interface : 'a t -> _t * 'a t option

  val split_at_functor : 'a t -> _functor t * 'a t option

  val drop : 'a t -> 'b t -> 'b t option

  (* PREDICATES *)

  val variety : 'a t -> variety
  val in_functor : 'a t -> bool

  val equal : 'a t -> 'a t -> bool

  (* TRANSFORMERS *)

  val to_longident : 'a t -> Longident.t

  val promote : ?by:int -> 'a t -> 'a t
  val demote : ?by:int -> 'a t -> 'a t

  (* OUTPUT *)

  val pp : 'a t Format.printer
  val _pp : _t Format.printer

end

type 'a t = string option * 'a Chain.t
type _t = string option * Chain._t

val is_ident : string -> bool
val is_lowercase_ident : string -> bool
val is_capitalized_ident : string -> bool

val parse_ident : (string, unit) MParser.t
val parse_lowercase_ident : (string, unit) MParser.t
val parse_capitalized_ident : (string, unit) MParser.t

val of_string : string -> _t

val pp : 'a t Format.printer
val _pp : _t Format.printer

val lib : 'a t -> string option
val chain : 'a t -> 'a Chain.t

val equal : 'a t -> 'a t -> bool