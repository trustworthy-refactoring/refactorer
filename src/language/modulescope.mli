open Containers

open Compiler
open Asttypes

open Typedtree_views

type parameter_frame =
  (Ident.t * Typedtree.module_type) Option.t * Typedtree.module_type list

type module_type_constraint =
  Path.t * Longident.t loc * Typedtree.with_constraint

type module_constraint =
  | InStr of Typedtree.module_type list
  | InSig of module_type_constraint list list

type frame = private
  | Module of Ident.t option * module_constraint * parameter_frame list
  | ModuleType of
      Ident.t option * module_type_constraint list list * parameter_frame list
  | Parameter of Ident.t

type t = (string * Typedtree.signature Option.t) * frame list
(* The frame list is a stack, so the innermost frame is at the top. *)

val module_name : t -> string
(** [module_name ((n, _), _) = n] *)

val module_sig : t -> Typedtree.signature Option.t
(** [module_sig ((_, sig), _) = sig] *)

val default_toplevel_name : string
(** A default name for the the toplevel module of a scope. *)

val empty : t
(** The empty module scope with [default_toplevel_name]. *)

val peek : t -> frame
(** [peek s] returns the top scope frame of [s], or raises Invalid_argument if
    the frame list is empty. *)

val peek_module : t -> Ident.t option * module_constraint * parameter_frame list
val peek_module_type :
  t -> Ident.t option * module_type_constraint list list * parameter_frame list
val peek_params : t -> parameter_frame list

val push_param : parameter_frame -> t -> t

val frame_id : frame -> Ident.t Option.t

val frame_is_naked : frame -> bool

val enter_module_binding : 'a module_view -> t -> t * 'a module_expr_view

val enter_include : 'a include_view -> t -> t * 'a module_expr_view

val enter_module_type_declaration :
  Typedtree.module_type_declaration -> t -> t * Typedtree.module_type Option.t

val enter_param : Ident.t -> t -> t

val find_param : Ident.t -> t -> (parameter_frame * t) Option.t

val in_frame_sigs : 'a Identifier.Chain.t -> frame -> bool

val to_identifier :
  ?check_sigs:bool -> ?id_tail:Identifier.Chain._t -> t
    -> Identifier.Chain._t Option.t

val pp_frame : frame Format.printer