open Containers

open Compiler

module Base = struct

  type impl = Impl
  type intf = Intf

  type _ sort =
    | Impl : impl sort
    | Intf : intf sort

  type _value = Value
  type _structure = Structure
  type _structure_type = StructureType
  type _functor = Functor
  type _functor_type = FunctorType

  type _parameter = Parameter

  type _ t =
    | Value         :                 _value          t
    | Parameter     :                 _parameter      t
    | Functor       :                 _functor        t
    | FunctorType   :                 _functor_type   t
    | Structure     :                 _structure      t
    | StructureType :                 _structure_type t

  type _t = Ex : _ t -> _t

  let ex v = Ex v

  let equal = fun (type a b) (x : a t) (y : b t) ->
    match x, y with
    | Value, Value -> true
    | Parameter, Parameter -> true
    | Structure, Structure -> true
    | StructureType, StructureType -> true
    | Functor, Functor -> true
    | FunctorType, FunctorType -> true
    | _ -> false

  let pp : type a . (a t) Format.printer =
    fun fmt kind ->
      Format.fprintf fmt "%s"
        begin match kind with
        | Value -> "value"
        | Parameter -> "parameter"
        | Functor -> "functor"
        | FunctorType -> "functor type"
        | Structure -> "structure"
        | StructureType -> "structure type"
        end

  let _pp fmt (Ex v) = pp fmt v

  exception KindMismatch of string

end

open Base

module type Compound_Sig = sig
  type 'a sub_t
  val lift : 'a sub_t -> 'a t
  val pp : 'a sub_t Format.printer
  type _sub_t = Ex : 'a sub_t -> _sub_t
  val _lift : _sub_t -> _t
end

module Module = struct
  type _ sub_t =
    | S : _structure sub_t
    | F : _functor   sub_t
  let lift (type a) (m : a sub_t) : a t =
    match m with
    | S -> Structure
    | F -> Functor
  let pp fmt m = pp fmt (lift m)
  type _sub_t = Ex : 'a sub_t -> _sub_t
  let _lift (Ex m) = ex (lift m)
  module type T0 = sig
    type 'a data
    type module_t =
      | S of _structure data
      | F of _functor data
  end
  (* module type S0 = sig
    include T0
    val lift_module : t data -> module_t
  end *)
  module Make_T0 (X : sig type 'a data end) = struct
    type module_t =
      | S of _structure X.data
      | F of _functor X.data
  end
  module type T1 = sig
    type ('a, 'b) data
    type 'a module_t =
      | S of ('a, _structure) data
      | F of ('a, _functor) data
  end
  (* module type S1 = sig
    include T1
    val lift_module : ('a, t) data -> 'a module_t
  end *)
  module Make_T1 (X : sig type ('a, 'b) data end) = struct
    type 'a module_t =
      | S of ('a, _structure) X.data
      | F of ('a, _functor) X.data
  end
end

module ModuleType = struct
  type _ sub_t =
    | ST : _structure_type sub_t
    | FT : _functor_type   sub_t
  let lift (type a) (m : a sub_t) : a t =
    match m with
    | ST -> StructureType
    | FT -> FunctorType
  let pp fmt m = pp fmt (lift m)
  type _sub_t = Ex : 'a sub_t -> _sub_t
  let _lift (Ex m) = ex (lift m)
  let type_of =
    function
    | Ex ST -> Module.(Ex S)
    | Ex FT -> Module.(Ex F)
  module type T0 = sig
    type 'a data
    type module_type_t =
      | ST of _structure_type data
      | FT of _functor_type data
  end
  (* module type S0 = sig
    include T0
    val lift_module_type : t data -> module_type_t
  end *)
  module Make_T0 (X : sig type 'a data end) = struct
    type module_type_t =
      | ST of _structure_type X.data
      | FT of _functor_type X.data
  end
  module type T1 = sig
    type ('a, 'b) data
    type 'a module_type_t =
      | ST of ('a, _structure_type) data
      | FT of ('a, _functor_type) data
  end
  (* module type S1 = sig
    include T1
    val lift_module_type : ('a, t) data -> 'a module_type_t
  end *)
  module Make_T1 (X : sig type ('a, 'b) data end) = struct
    type 'a module_type_t =
      | ST of ('a, _structure_type) X.data
      | FT of ('a, _functor_type) X.data
  end
end