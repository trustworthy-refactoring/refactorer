open Containers

open Compiler

open Types

open Elements
open Elements.Base
open Identifier
open Atom

type _ module_type =
  | ST : signature                                              -> _structure_type module_type
  | FT : Ident.t * Types.module_type option * Types.module_type -> _functor_type   module_type

type _module_type = Ex : 'a module_type -> _module_type

type 'a module_type_view =
  | Mty_immediate of 'a module_type
  | Mty_ident     of 'a ModuleType.sub_t * Path.t
  | Mty_alias     of 'a ModuleType.sub_t * (alias_presence * Path.t)

type 'a module_declaration_view =
  {
    md_type       : 'a module_type_view ;
    md_attributes : Parsetree.attributes ;
    md_loc        : Location.t ;
  }

type 'a modtype_declaration_view =
  {
    mtd_type       : 'a module_type_view option ;
    mtd_attributes : Parsetree.attributes ;
    mtd_loc        : Location.t ;
  }

type parameter_view =
  | Functor of Types.module_type option * int

(* We call this an item element view because here we will only consider elements
   that can be structure/signature items. *)
type _ item_element_view =
  | Value         : Ident.t * value_description                                    -> _value          item_element_view
  | Structure     : Ident.t * _structure_type module_declaration_view * rec_status -> _structure      item_element_view
  | Functor       : Ident.t * _functor_type   module_declaration_view * rec_status -> _functor        item_element_view
  | StructureType : Ident.t * _structure_type modtype_declaration_view             -> _structure_type item_element_view
  | FunctorType   : Ident.t * _functor_type   modtype_declaration_view             -> _functor_type   item_element_view
  (* Eventually, we will also have types, exceptions, and classes (attributes too?) *)

val mk_module_type_view :
  'a ModuleType.sub_t * Types.module_type -> 'a module_type_view
val mk_item_view :
  (string, 'a) Atom.t -> signature_item -> 'a item_element_view option

val item_ident : 'a item_element_view -> Ident.t