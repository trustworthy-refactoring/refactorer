open Containers

open Compiler

open Types

open Elements
open Elements.Base
open Identifier
open Atom

type _ module_type =
  | ST : signature                                              -> _structure_type module_type
  | FT : Ident.t * Types.module_type option * Types.module_type -> _functor_type   module_type

type _module_type = Ex : 'a module_type -> _module_type

type 'a module_type_view =
  | Mty_immediate of 'a module_type
  | Mty_ident     of 'a ModuleType.sub_t * Path.t
  | Mty_alias     of 'a ModuleType.sub_t * (alias_presence * Path.t)

type 'a module_declaration_view =
  {
    md_type       : 'a module_type_view ;
    md_attributes : Parsetree.attributes ;
    md_loc        : Location.t ;
  }

type 'a modtype_declaration_view =
  {
    mtd_type       : 'a module_type_view option ;
    mtd_attributes : Parsetree.attributes ;
    mtd_loc        : Location.t ;
  }

type parameter_view =
  | Functor of Types.module_type option * int

type _ item_element_view =
  | Value         : Ident.t * value_description                                    -> _value          item_element_view
  | Structure     : Ident.t * _structure_type module_declaration_view * rec_status -> _structure      item_element_view
  | Functor       : Ident.t * _functor_type   module_declaration_view * rec_status -> _functor        item_element_view
  | StructureType : Ident.t * _structure_type modtype_declaration_view             -> _structure_type item_element_view
  | FunctorType   : Ident.t * _functor_type   modtype_declaration_view             -> _functor_type   item_element_view


let mk_module_type_view
      (type a) ((sort : a ModuleType.sub_t), (mty : Types.module_type))
    : a module_type_view =
  match sort, mty with
  | ModuleType.ST, Mty_signature _sig ->
    Mty_immediate (ST _sig)
  | ModuleType.FT, Mty_functor (x, x_sig, _sig) ->
    Mty_immediate (FT (x, x_sig, _sig))
  | _, Mty_ident p ->
    Mty_ident (sort, p)
  | _, Mty_alias (present, p) ->
    Mty_alias (sort, (present, p))
  | _ ->
    raise
      (KindMismatch
        (Format.sprintf "module type is not of sort %a"
          Elements.ModuleType.pp sort))

let mk_module_declaration_view
      (type a) (sort : a ModuleType.sub_t)
      Types.{ md_type; md_attributes; md_loc; }
    : a module_declaration_view =
  let md_type = mk_module_type_view (sort, md_type) in
  { md_type; md_attributes; md_loc }

let mk_modtype_declaration_view
      (type a) (sort : a ModuleType.sub_t)
      Types.{ mtd_type; mtd_attributes; mtd_loc; }
    : a modtype_declaration_view =
  let mtd_type = Option.map (Fun.curry mk_module_type_view sort) mtd_type in
  { mtd_type; mtd_attributes; mtd_loc }

let rec mk_functor_param idx mty =
  match mk_module_type_view (ModuleType.FT, mty) with
  | Mty_immediate FT (x, x_sig, _) when idx = 1 ->
    Some (x, x_sig)
  | Mty_immediate FT (_, _, _sig) ->
    mk_functor_param (idx - 1) _sig
  | _ ->
    None
  | exception KindMismatch _ ->
    None

let mk_item_view
    (type a) (a : (string, a) Atom.t) item : a item_element_view option =
  try match a, item with
  | Value v, Sig_value (id, vd)
      when v = id.Ident.name ->
    Some (Value (id, vd))
  | Structure m, Sig_module (id, md, _rec)
      when m = id.Ident.name ->
    let mdv = mk_module_declaration_view ModuleType.ST md in
    Some (Structure (id, mdv, _rec))
  | Functor m, Sig_module (id, md, _rec)
      when m = id.Ident.name ->
    let mdv = mk_module_declaration_view ModuleType.FT md in
    Some (Functor (id, mdv, _rec))
  | StructureType mt, Sig_modtype (id, mtd)
      when mt = id.Ident.name ->
    let mtdv = mk_modtype_declaration_view ModuleType.ST mtd in
    Some (StructureType (id, mtdv))
  | FunctorType mt, Sig_modtype (id, mtd)
      when mt = id.Ident.name ->
    let mtdv = mk_modtype_declaration_view ModuleType.FT mtd in
    Some (FunctorType (id, mtdv))
  (* | Parameter (p, Some idx), Sig_module (_, { md_type = mty; _}, _) ->
    (mk_functor_param idx mty)
    |> Option.map @@ fun (x, x_sig)
    -> let param_view : parameter_view = Functor (x_sig, idx)
    in Parameter (x, param_view)
  | Parameter (p, Some idx), Sig_modtype (_, { mtd_type = Some mty }) ->
    (mk_functor_param idx mty)
    |> Option.map @@ fun (x, x_sig)
    -> let param_view : parameter_view = Functor (x_sig, idx)
    in Parameter (x, param_view) *)
  | _ ->
    None
  with KindMismatch _ ->
    None

let item_ident (type a) (view : a item_element_view) =
  match view with
  | Value (id, _) -> id
  | Structure (id, _, _) -> id
  | Functor (id, _, _) -> id
  | StructureType (id, _) -> id
  | FunctorType (id, _) -> id
