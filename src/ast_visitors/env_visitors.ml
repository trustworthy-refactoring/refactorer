
open Compiler

module Base = Env

(* It seems that we just need Env.t, which is actually an abstract type, for Typedtree
   and so we'll leave it opaque for now. If it turns out that we need to do traversals
   over typing environments then we can think about writing custom wrapper visitors. *)
type env_t = Base.t [@opaque]

[@@deriving
    visitors { variety = "iter"; }
  , visitors { variety = "map"; }
  , visitors { variety = "reduce"; }

  , visitors { variety = "iter2"; concrete = true; }
]