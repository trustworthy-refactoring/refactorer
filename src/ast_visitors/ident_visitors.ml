
open Compiler

module Base = Ident

type ident_t = Base.t [@opaque]

[@@deriving
    visitors { variety = "iter"; }
  , visitors { variety = "map"; }
  , visitors { variety = "reduce"; }

  , visitors { variety = "iter2"; concrete = true; }
]