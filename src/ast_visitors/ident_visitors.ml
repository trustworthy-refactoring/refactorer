
open Compiler

module Base = Ident

type ident_t = Base.t =
  {
    stamp: int;
    name: string;
    mutable flags: int
  }

[@@deriving
    visitors { variety = "iter"; }
  , visitors { variety = "map"; }
  , visitors { variety = "reduce"; }

  , visitors { variety = "iter2"; concrete = true; }
]