(* --------------------------------------------------------------------------
   Occurrences of the following types in type definitions have been changed
   to use the types indicated in parentheses so that the visitor generation
   code creates calls to the correct method.

       Location.t               (Location_visitors.location_t)
       Longident.t              (Longident_visitors.longident_t)
       Path.t                   (Path_visitors.path_t)
       Ident.t                  (Ident_visitors.ident_t)

       Parsetree.attributes     (Parsetree_visitors.pt_attributes)

   All local types have been prefixed with "ty_" as a namespace; this is to
   avoid clashses in generating names for methods visiting the types in the
   Parsetree and Typedtree modules.
   -------------------------------------------------------------------------- *)

open Compiler
open Asttypes

module Base = Types

type ty_type_expr = Base.type_expr =
  { mutable desc: ty_type_desc;
    mutable level: int;
    id: int }

and ty_type_desc = Base.type_desc =
  | Tvar of string option
  | Tarrow of arg_label * ty_type_expr * ty_type_expr * ty_commutable
  | Ttuple of ty_type_expr list
  | Tconstr of Path_visitors.path_t * ty_type_expr list * ty_abbrev_memo ref
  | Tobject of ty_type_expr * (Path_visitors.path_t * ty_type_expr list) option ref
  | Tfield of string * ty_field_kind * ty_type_expr * ty_type_expr
  | Tnil
  | Tlink of ty_type_expr
  | Tsubst of ty_type_expr         (* for copying *)
  | Tvariant of ty_row_desc
  | Tunivar of string option
  | Tpoly of ty_type_expr * ty_type_expr list
  | Tpackage of Path_visitors.path_t * Longident_visitors.longident_t list * ty_type_expr list
and ty_row_desc = Base.row_desc =
    { row_fields: (label * ty_row_field) list;
      row_more: ty_type_expr;
      row_bound: unit; (* kept for compatibility *)
      row_closed: bool;
      row_fixed: bool;
      row_name: (Path_visitors.path_t * ty_type_expr list) option }

and ty_row_field = Base.row_field =
    Rpresent of ty_type_expr option
  | Reither of bool * ty_type_expr list * bool * ty_row_field option ref
  | Rabsent

and ty_abbrev_memo = Base.abbrev_memo =
  | Mnil (** No known abbrevation *)
  | Mcons of private_flag * Path_visitors.path_t * ty_type_expr * ty_type_expr * ty_abbrev_memo
  | Mlink of ty_abbrev_memo ref

and ty_field_kind = Base.field_kind =
    Fvar of ty_field_kind option ref
  | Fpresent
  | Fabsent

and ty_commutable = Base.commutable =
    Cok
  | Cunknown
  | Clink of ty_commutable ref

and ty_value_description = Base.value_description =
  { val_type: ty_type_expr;                (* Type of the value *)
    val_kind: ty_value_kind;
    val_loc: Location_visitors.location_t;
    val_attributes: Parsetree_visitors.pt_attributes;
   }

and ty_value_kind = Base.value_kind =
    Val_reg                             (* Regular value *)
  | Val_prim of Primitive.description   (* Primitive *)
  | Val_ivar of mutable_flag * string   (* Instance variable (mutable ?) *)
  | Val_self of
      ((Ident_visitors.ident_t * ty_type_expr) Base.Meths.t ref [@opaque]) *
      ((Ident_visitors.ident_t * mutable_flag * virtual_flag * ty_type_expr)
        Base.Vars.t ref [@opaque]) *
      string * ty_type_expr      (* Self *)
  | Val_anc of (string * Ident_visitors.ident_t) list * string
                                        (* Ancestor *)
  | Val_unbound                         (* Unbound variable *)

and variance_t = (Base.Variance.t [@opaque])
and variance_f = Base.Variance.f =
    May_pos | May_neg | May_weak | Inj | Pos | Neg | Inv

and ty_type_declaration = Base.type_declaration =
  { type_params: ty_type_expr list;
    type_arity: int;
    type_kind: ty_type_kind;
    type_private: private_flag;
    type_manifest: ty_type_expr option;
    type_variance: variance_t list;
    type_newtype_level: (int * int) option;
    type_loc: Location_visitors.location_t;
    type_attributes: Parsetree_visitors.pt_attributes;
    type_immediate: bool; (* true iff type should not be a pointer *)
    type_unboxed: ty_unboxed_status;
  }

and ty_type_kind = Base.type_kind =
    Type_abstract
  | Type_record of ty_label_declaration list  * ty_record_representation
  | Type_variant of ty_constructor_declaration list
  | Type_open

and ty_record_representation = Base.record_representation =
    Record_regular                      (* All fields are boxed / tagged *)
  | Record_float                        (* All fields are floats *)
  | Record_unboxed of bool    (* Unboxed single-field record, inlined or not *)
  | Record_inlined of int               (* Inlined record *)
  | Record_extension                    (* Inlined record under extension *)

and ty_label_declaration = Base.label_declaration =
  {
    ld_id: Ident_visitors.ident_t;
    ld_mutable: mutable_flag;
    ld_type: ty_type_expr;
    ld_loc: Location_visitors.location_t;
    ld_attributes: Parsetree_visitors.pt_attributes;
  }

and ty_constructor_declaration = Base.constructor_declaration =
  {
    cd_id: Ident_visitors.ident_t;
    cd_args: ty_constructor_arguments;
    cd_res: ty_type_expr option;
    cd_loc: Location_visitors.location_t;
    cd_attributes: Parsetree_visitors.pt_attributes;
  }

and ty_constructor_arguments = Base.constructor_arguments =
  | Cstr_tuple of ty_type_expr list
      [@name "ty_Cstr_tuple"]
  | Cstr_record of ty_label_declaration list
      [@name "ty_Cstr_record"]
  (* Visitors for these constructors are renamed to avoid clashes with Typedtree *)

and ty_unboxed_status = Base.unboxed_status =
  private {
    unboxed: bool;
    default: bool; (* True for unannotated unboxable types. *)
  } [@@build
      fun unboxed default ->
        match unboxed, default with
        | false, false -> Base.unboxed_false_default_false
        | false, true  -> Base.unboxed_false_default_true
        | true, false  -> Base.unboxed_true_default_false
        | true, true   -> Base.unboxed_true_default_true
    ]

and ty_extension_constructor = Base.extension_constructor =
  {
    ext_type_path: Path_visitors.path_t;
    ext_type_params: ty_type_expr list;
    ext_args: ty_constructor_arguments;
    ext_ret_type: ty_type_expr option;
    ext_private: private_flag;
    ext_loc: Location_visitors.location_t;
    ext_attributes: Parsetree_visitors.pt_attributes;
  }

and ty_type_transparence = Base.type_transparence =
    Type_public      (* unrestricted expansion *)
  | Type_new         (* "new" type *)
  | Type_private     (* private type *)

and concr_t = (Base.Concr.t [@opaque])

and ty_class_type = Base.class_type =
    Cty_constr of Path_visitors.path_t * ty_type_expr list * ty_class_type
  | Cty_signature of ty_class_signature
  | Cty_arrow of arg_label * ty_type_expr * ty_class_type

and ty_class_signature = Base.class_signature =
  { csig_self: ty_type_expr;
    csig_vars:
      ((mutable_flag * virtual_flag * ty_type_expr) Base.Vars.t [@opaque]);
    csig_concr: concr_t;
    csig_inher: (Path_visitors.path_t * ty_type_expr list) list }

and ty_class_declaration = Base.class_declaration =
  { cty_params: ty_type_expr list;
    mutable cty_type: ty_class_type;
    cty_path: Path_visitors.path_t;
    cty_new: ty_type_expr option;
    cty_variance: variance_t list;
    cty_loc: Location_visitors.location_t;
    cty_attributes: Parsetree_visitors.pt_attributes;
  }

and ty_class_type_declaration = Base.class_type_declaration =
  { clty_params: ty_type_expr list;
    clty_type: ty_class_type;
    clty_path: Path_visitors.path_t;
    clty_variance: variance_t list;
    clty_loc: Location_visitors.location_t;
    clty_attributes: Parsetree_visitors.pt_attributes;
  }

and ty_module_type = Base.module_type =
    Mty_ident of Path_visitors.path_t
  | Mty_signature of ty_signature
  | Mty_functor of Ident_visitors.ident_t * ty_module_type option * ty_module_type
  | Mty_alias of ty_alias_presence * Path_visitors.path_t

and ty_alias_presence = Base.alias_presence =
  | Mta_present
  | Mta_absent

and ty_signature = ty_signature_item list

and ty_signature_item = Base.signature_item =
    Sig_value of Ident_visitors.ident_t * ty_value_description
  | Sig_type of Ident_visitors.ident_t * ty_type_declaration * ty_rec_status
  | Sig_typext of Ident_visitors.ident_t * ty_extension_constructor * ty_ext_status
  | Sig_module of Ident_visitors.ident_t * ty_module_declaration * ty_rec_status
  | Sig_modtype of Ident_visitors.ident_t * ty_modtype_declaration
  | Sig_class of Ident_visitors.ident_t * ty_class_declaration * ty_rec_status
  | Sig_class_type of Ident_visitors.ident_t * ty_class_type_declaration * ty_rec_status

and ty_module_declaration = Base.module_declaration =
  {
    md_type: ty_module_type;
    md_attributes: Parsetree_visitors.pt_attributes;
    md_loc: Location_visitors.location_t;
  }

and ty_modtype_declaration = Base.modtype_declaration =
  {
    mtd_type: ty_module_type option;  (* None: abstract *)
    mtd_attributes: Parsetree_visitors.pt_attributes;
    mtd_loc: Location_visitors.location_t;
  }

and ty_rec_status = Base.rec_status =
    Trec_not                   (* first in a nonrecursive group *)
  | Trec_first                 (* first in a recursive group *)
  | Trec_next                  (* not first in a recursive/nonrecursive group *)

and ty_ext_status = Base.ext_status =
    Text_first                     (* first constructor in an extension *)
  | Text_next                      (* not first constructor in an extension *)
  | Text_exception


and ty_constructor_description = Base.constructor_description =
  { cstr_name: string;                  (* Constructor name *)
    cstr_res: ty_type_expr;                (* Type of the result *)
    cstr_existentials: ty_type_expr list;  (* list of existentials *)
    cstr_args: ty_type_expr list;          (* Type of the arguments *)
    cstr_arity: int;                    (* Number of arguments *)
    cstr_tag: ty_constructor_tag;          (* Tag for heap blocks *)
    cstr_consts: int;                   (* Number of constant constructors *)
    cstr_nonconsts: int;                (* Number of non-const constructors *)
    cstr_normal: int;                   (* Number of non generalized constrs *)
    cstr_generalized: bool;             (* Constrained return type? *)
    cstr_private: private_flag;         (* Read-only constructor? *)
    cstr_loc: Location_visitors.location_t;
    cstr_attributes: Parsetree_visitors.pt_attributes;
    cstr_inlined: ty_type_declaration option;
   }

and ty_constructor_tag = Base.constructor_tag =
    Cstr_constant of int                (* Constant constructor (an int) *)
  | Cstr_block of int                   (* Regular constructor (a block) *)
  | Cstr_unboxed                        (* Constructor of an unboxed type *)
  | Cstr_extension of Path_visitors.path_t * bool
                                        (* Extension constructor
                                           true if a constant false if a block*)

and ty_label_description = Base.label_description =
  { lbl_name: string;                   (* Short name *)
    lbl_res: ty_type_expr;                 (* Type of the result *)
    lbl_arg: ty_type_expr;                 (* Type of the argument *)
    lbl_mut: mutable_flag;              (* Is this a mutable field? *)
    lbl_pos: int;                       (* Position in block *)
    lbl_all: ty_label_description array;   (* All the labels in this type *)
    lbl_repres: ty_record_representation;  (* Representation for this record *)
    lbl_private: private_flag;          (* Read-only field? *)
    lbl_loc: Location_visitors.location_t;
    lbl_attributes: Parsetree_visitors.pt_attributes;
  }

[@@deriving
    visitors {
        variety = "iter"
      ; polymorphic = true; monomorphic = ["'env"]
      ; ancestors = [
            "Asttype_visitors.iter"
          ; "Path_visitors.iter"
          ; "Primitive_visitors.iter"
          ; "Location_visitors.iter"
          ; "Longident_visitors.iter"
          ; "Parsetree_visitors.iter"
        ]
    }
  , visitors {
        variety = "map"
      ; polymorphic = true; monomorphic = ["'env"]
      ; ancestors = [
            "Asttype_visitors.map"
          ; "Path_visitors.map"
          ; "Primitive_visitors.map"
          ; "Location_visitors.map"
          ; "Longident_visitors.map"
          ; "Parsetree_visitors.map"
        ]
    }
  , visitors {
        variety = "reduce"
      ; polymorphic = true; monomorphic = ["'env"]
      ; ancestors = [
            "Asttype_visitors.reduce"
          ; "Path_visitors.reduce"
          ; "Primitive_visitors.reduce"
          ; "Location_visitors.reduce"
          ; "Longident_visitors.reduce"
          ; "Parsetree_visitors.reduce"
        ]
    }

  , visitors {
        variety = "iter2"
      ; polymorphic = true; monomorphic = ["'env"]
      ; concrete = true
      ; ancestors = [
            "Asttype_visitors.iter2"
          ; "Path_visitors.iter2"
          ; "Primitive_visitors.iter2"
          ; "Location_visitors.iter2"
          ; "Longident_visitors.iter2"
          ; "Parsetree_visitors.iter2"
        ]
    }
]