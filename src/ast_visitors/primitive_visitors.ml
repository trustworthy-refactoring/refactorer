
open Compiler

module Base = Primitive

type boxed_integer = Base.boxed_integer =
    Pnativeint | Pint32 | Pint64
and native_repr = Base.native_repr =
  | Same_as_ocaml_repr
  | Unboxed_float
  | Unboxed_integer of boxed_integer
  | Untagged_int
and description = Base.description = private {
    prim_name: string;         (* Name of primitive  or C function *)
    prim_arity: int;           (* Number of arguments *)
    prim_alloc: bool;          (* Does it allocates or raise? *)
    prim_native_name: string;  (* Name of C function for the nat. code gen. *)
    prim_native_repr_args: native_repr list;
    prim_native_repr_res: native_repr }
      [@@build
        fun name arity alloc native_name native_repr_args native_repr_res ->
          Base.make ~name ~alloc ~native_name ~native_repr_args ~native_repr_res]

[@@deriving
    visitors { variety = "iter"; }
  , visitors { variety = "map"; }
  , visitors { variety = "reduce"; }

  , visitors { variety = "iter2"; concrete = true; }
]