
open Compiler

module Base = Path

type path_t = Base.t =
  | Pident of Ident_visitors.ident_t
#if OCAML_MINOR < 8
  | Pdot of path_t * string * int
#else
  | Pdot of path_t * string
#endif
  | Papply of path_t * path_t

and typath = Base.typath =
  | Regular of path_t
  | Ext of path_t * string
  | LocalExt of Ident_visitors.ident_t
  | Cstr of path_t * string

[@@deriving
    visitors { variety = "iter"; ancestors = [ "Ident_visitors.iter" ] }
  , visitors { variety = "map"; ancestors = [ "Ident_visitors.map" ] }
  , visitors { variety = "reduce"; ancestors = [ "Ident_visitors.reduce" ] }

  , visitors { variety = "iter2"; concrete = true; ancestors = [ "Ident_visitors.iter2" ] }
]