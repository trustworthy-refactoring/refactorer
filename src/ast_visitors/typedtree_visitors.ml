
(* --------------------------------------------------------------------------
   Occurrences of the following types in type definitions have been changed
   to use the types indicated in parentheses so that the visitor generation
   code creates calls to the correct method.

       Location.t               (Location_visitors.location_t)
       Longident.t              (Longident_visitors.longident_t)
       Path.t                   (Path_visitors.path_t)
       Ident.t                  (Ident_visitors.ident_t)
       Env.t                    (Env_visitors.env_t)

       Parsetree.attributes     (Parsetree_visitors.pt_attributes)
       Parsetree.pattern        (Parsetree_visitors.pt_pattern)

       Concr.t                  (Types_visitors.concr_t)
       Meths.t                  (Types_visitors.meth_t)

       Types.type_expr                (Types_visitors.ty_type_expr)
       Types.constructor_description  (Types_visitors.ty_constructor_description)
       Types.value_description
       Types.label_description
       Types.record_representation
       Types.class_declaration
       Types.class_type
       Types.class_signature
       Types.module_type
       Types.signature
       Types.type_declaration
       Types.extension_constructor
       Types.class_type_declaration

   All local types have been prefixed with "tt_" as a namespace; this is to
   avoid clashses in generating names for methods visiting the types in the
   Parsetree and Types modules.
   -------------------------------------------------------------------------- *)

open Compiler

open Asttypes

module Base = Typedtree

type tt_partial = Base.partial =
  Partial | Total

and tt_attribute = Parsetree_visitors.pt_attribute
and tt_attributes = tt_attribute list
and tt_pattern = Base.pattern =
  { pat_desc: tt_pattern_desc;
    pat_loc: Location_visitors.location_t;
    pat_extra : (tt_pat_extra * Location_visitors.location_t * tt_attributes) list;
    pat_type: Types_visitors.ty_type_expr [@opaque];
    mutable pat_env: Env_visitors.env_t;
    pat_attributes: tt_attributes;
   }

and tt_pat_extra = Base.pat_extra =
  | Tpat_constraint of tt_core_type
  | Tpat_type of Path_visitors.path_t * Longident_visitors.longident_t loc
  | Tpat_open of Path_visitors.path_t
                   * Longident_visitors.longident_t loc * Env_visitors.env_t
  | Tpat_unpack

and tt_pattern_desc = Base.pattern_desc =
    Tpat_any
  | Tpat_var of Ident_visitors.ident_t * string loc
  | Tpat_alias of tt_pattern * Ident_visitors.ident_t * string loc
  | Tpat_constant of constant
  | Tpat_tuple of tt_pattern list
  | Tpat_construct of
      Longident_visitors.longident_t loc
        * (Types_visitors.ty_constructor_description [@opaque])
        * tt_pattern list
  | Tpat_variant of
      label * tt_pattern option
        * (Types_visitors.ty_row_desc ref [@opaque])
  | Tpat_record of
      (Longident_visitors.longident_t loc
        * (Types_visitors.ty_label_description [@opaque]) * tt_pattern) list *
        closed_flag
  | Tpat_array of tt_pattern list
  | Tpat_or of
      tt_pattern * tt_pattern * (Types_visitors.ty_row_desc [@opaque]) option
  | Tpat_lazy of tt_pattern

and tt_expression = Base.expression =
  { exp_desc: tt_expression_desc;
    exp_loc: Location_visitors.location_t;
    exp_extra: (tt_exp_extra * Location_visitors.location_t * tt_attributes) list;
    exp_type: (Types_visitors.ty_type_expr [@opaque]);
    exp_env: Env_visitors.env_t;
    exp_attributes: tt_attributes;
   }

and tt_exp_extra = Base.exp_extra =
  | Texp_constraint of tt_core_type
  | Texp_coerce of tt_core_type option * tt_core_type
  | Texp_open of
      override_flag * Path_visitors.path_t * Longident_visitors.longident_t loc
        * Env_visitors.env_t
  | Texp_poly of tt_core_type option
  | Texp_newtype of string

and tt_expression_desc = Base.expression_desc =
    Texp_ident of
      Path_visitors.path_t * Longident_visitors.longident_t loc
        * (Types_visitors.ty_value_description [@opaque])
  | Texp_constant of constant
  | Texp_let of rec_flag * tt_value_binding list * tt_expression
  | Texp_function of arg_label * tt_case list * tt_partial
  | Texp_apply of tt_expression * (arg_label * tt_expression option) list
  | Texp_match of tt_expression * tt_case list * tt_case list * tt_partial
  | Texp_try of tt_expression * tt_case list
  | Texp_tuple of tt_expression list
  | Texp_construct of
      Longident_visitors.longident_t loc
        * (Types_visitors.ty_constructor_description [@opaque])
        * tt_expression list
  | Texp_variant of label * tt_expression option
  | Texp_record of {
      fields : (
        (Types_visitors.ty_label_description [@opaque])
          * tt_record_label_definition ) array;
      representation : (Types_visitors.ty_record_representation [@opaque]);
      extended_expression : tt_expression option;
    }
  | Texp_field of
      tt_expression * Longident_visitors.longident_t loc
        * (Types_visitors.ty_label_description [@opaque])
  | Texp_setfield of
      tt_expression * Longident_visitors.longident_t loc
        * (Types_visitors.ty_label_description [@opaque]) * tt_expression
  | Texp_array of tt_expression list
  | Texp_ifthenelse of tt_expression * tt_expression * tt_expression option
  | Texp_sequence of tt_expression * tt_expression
  | Texp_while of tt_expression * tt_expression
  | Texp_for of
      Ident_visitors.ident_t * Parsetree_visitors.pt_pattern * tt_expression
        * tt_expression * direction_flag * tt_expression
  | Texp_send of tt_expression * meth * tt_expression option
  | Texp_new of
      Path_visitors.path_t * Longident_visitors.longident_t loc
        * (Types_visitors.ty_class_declaration [@opaque])
  | Texp_instvar of Path_visitors.path_t * Path_visitors.path_t * string loc
  | Texp_setinstvar of
      Path_visitors.path_t * Path_visitors.path_t * string loc * tt_expression
  | Texp_override of Path_visitors.path_t
      * (Path_visitors.path_t * string loc * tt_expression) list
  | Texp_letmodule of
      Ident_visitors.ident_t * string loc * tt_module_expr * tt_expression
  | Texp_letexception of tt_extension_constructor * tt_expression
  | Texp_assert of tt_expression
  | Texp_lazy of tt_expression
  | Texp_object of tt_class_structure * string list
  | Texp_pack of tt_module_expr
  | Texp_unreachable
  | Texp_extension_constructor of
      Longident_visitors.longident_t loc * Path_visitors.path_t

and meth = Base.meth =
    Tmeth_name of string
  | Tmeth_val of Ident_visitors.ident_t

and tt_case = Base.case =
    {
     c_lhs: tt_pattern;
     c_guard: tt_expression option;
     c_rhs: tt_expression;
    }

and tt_record_label_definition = Base.record_label_definition =
  | Kept of (Types_visitors.ty_type_expr [@opaque])
  | Overridden of Longident_visitors.longident_t loc * tt_expression

(* Value expressions for the class language *)

and tt_class_expr = Base.class_expr =
    {
     cl_desc: tt_class_expr_desc;
     cl_loc: Location_visitors.location_t;
     cl_type: (Types_visitors.ty_class_type [@opaque]);
     cl_env: Env_visitors.env_t;
     cl_attributes: tt_attributes;
    }

and tt_class_expr_desc = Base.class_expr_desc =
    Tcl_ident of
      Path_visitors.path_t * Longident_visitors.longident_t loc * tt_core_type list
  | Tcl_structure of tt_class_structure
  | Tcl_fun of arg_label * tt_pattern
      * (Ident_visitors.ident_t * string loc * tt_expression) list
      * tt_class_expr * tt_partial
  | Tcl_apply of tt_class_expr * (arg_label * tt_expression option) list
  | Tcl_let of rec_flag * tt_value_binding list
      * (Ident_visitors.ident_t * string loc * tt_expression) list * tt_class_expr
  | Tcl_constraint of
      tt_class_expr * tt_class_type option * string list * string list
        * (Types_visitors.concr_t [@opaque])

and tt_class_structure = Base.class_structure =
  {
   cstr_self: tt_pattern;
   cstr_fields: tt_class_field list;
   cstr_type: (Types_visitors.ty_class_signature [@opaque]);
   cstr_meths: (Ident_visitors.ident_t Types.Meths.t [@opaque]);
  }

and tt_class_field = Base.class_field =
   {
    cf_desc: tt_class_field_desc;
    cf_loc: Location_visitors.location_t;
    cf_attributes: tt_attributes;
  }

and tt_class_field_kind = Base.class_field_kind =
  | Tcfk_virtual of tt_core_type
  | Tcfk_concrete of override_flag * tt_expression

and tt_class_field_desc = Base.class_field_desc =
    Tcf_inherit of override_flag * tt_class_expr * string option
      * (string * Ident_visitors.ident_t) list
      * (string * Ident_visitors.ident_t) list
  | Tcf_val of string loc * mutable_flag * Ident_visitors.ident_t
      * tt_class_field_kind * bool
  | Tcf_method of string loc * private_flag * tt_class_field_kind
  | Tcf_constraint of tt_core_type * tt_core_type
  | Tcf_initializer of tt_expression
  | Tcf_attribute of tt_attribute

and tt_module_expr = Base.module_expr =
  { mod_desc: tt_module_expr_desc;
    mod_loc: Location_visitors.location_t;
    mod_type: (Types_visitors.ty_module_type [@opaque]);
    mod_env: Env_visitors.env_t;
    mod_attributes: tt_attributes;
   }

(** Annotations for [Tmod_constraint]. *)
and tt_module_type_constraint = Base.module_type_constraint =
  | Tmodtype_implicit
  | Tmodtype_explicit of tt_module_type

and tt_module_expr_desc = Base.module_expr_desc =
    Tmod_ident of Path_visitors.path_t * Longident_visitors.longident_t loc
  | Tmod_structure of tt_structure
  | Tmod_functor of
      Ident_visitors.ident_t * string loc * tt_module_type option
        * tt_module_expr
  | Tmod_apply of tt_module_expr * tt_module_expr * tt_module_coercion
  | Tmod_constraint of
      tt_module_expr * (Types_visitors.ty_module_type [@opaque])
        * tt_module_type_constraint * tt_module_coercion
  | Tmod_unpack of tt_expression * (Types_visitors.ty_module_type [@opaque])

and tt_structure = Base.structure = {
  str_items : tt_structure_item list;
  str_type : (Types_visitors.ty_signature [@opaque]);
  str_final_env : Env_visitors.env_t;
}

and tt_structure_item = Base.structure_item =
  { str_desc : tt_structure_item_desc;
    str_loc : Location_visitors.location_t;
    str_env : Env_visitors.env_t
  }

and tt_structure_item_desc = Base.structure_item_desc =
    Tstr_eval of tt_expression * tt_attributes
  | Tstr_value of rec_flag * tt_value_binding list
  | Tstr_primitive of tt_value_description
  | Tstr_type of rec_flag * tt_type_declaration list
  | Tstr_typext of tt_type_extension
  | Tstr_exception of tt_extension_constructor
  | Tstr_module of tt_module_binding
  | Tstr_recmodule of tt_module_binding list
  | Tstr_modtype of tt_module_type_declaration
  | Tstr_open of tt_open_description
  | Tstr_class of (tt_class_declaration * string list) list
  | Tstr_class_type of
      (Ident_visitors.ident_t * string loc * tt_class_type_declaration) list
  | Tstr_include of tt_include_declaration
  | Tstr_attribute of tt_attribute

and tt_module_binding = Base.module_binding =
    {
     mb_id: Ident_visitors.ident_t;
     mb_name: string loc;
     mb_expr: tt_module_expr;
     mb_attributes: tt_attributes;
     mb_loc: Location_visitors.location_t;
    }

and tt_value_binding = Base.value_binding =
  {
    vb_pat: tt_pattern;
    vb_expr: tt_expression;
    vb_attributes: tt_attributes;
    vb_loc: Location_visitors.location_t;
  }

and tt_module_coercion = Base.module_coercion =
    Tcoerce_none
  | Tcoerce_structure of
      (int * tt_module_coercion) list
        * (Ident_visitors.ident_t * int * tt_module_coercion) list
  | Tcoerce_functor of tt_module_coercion * tt_module_coercion
  | Tcoerce_primitive of tt_primitive_coercion
  | Tcoerce_alias of Path_visitors.path_t * tt_module_coercion

and tt_module_type = Base.module_type =
  { mty_desc: tt_module_type_desc;
    mty_type : (Types_visitors.ty_module_type [@opaque]);
    mty_env : Env_visitors.env_t;
    mty_loc: Location_visitors.location_t;
    mty_attributes: tt_attributes;
   }

and tt_module_type_desc = Base.module_type_desc =
    Tmty_ident of Path_visitors.path_t * Longident_visitors.longident_t loc
  | Tmty_signature of tt_signature
  | Tmty_functor of Ident_visitors.ident_t * string loc * tt_module_type option
      * tt_module_type
  | Tmty_with of tt_module_type *
      (Path_visitors.path_t
        * Longident_visitors.longident_t loc * tt_with_constraint) list
  | Tmty_typeof of tt_module_expr
  | Tmty_alias of Path_visitors.path_t * Longident_visitors.longident_t loc

and tt_primitive_coercion = Base.primitive_coercion =
  {
    pc_desc: Primitive.description;
    pc_type: (Types_visitors.ty_type_expr [@opaque]);
    pc_env: Env_visitors.env_t;
    pc_loc : Location_visitors.location_t;
  }

and tt_signature = Base.signature = {
  sig_items : tt_signature_item list;
  sig_type : (Types_visitors.ty_signature [@opaque]);
  sig_final_env : Env_visitors.env_t;
}

and tt_signature_item = Base.signature_item =
  { sig_desc: tt_signature_item_desc;
    sig_env : Env_visitors.env_t; (* BINANNOT ADDED *)
    sig_loc: Location_visitors.location_t }

and tt_signature_item_desc = Base.signature_item_desc =
    Tsig_value of tt_value_description
  | Tsig_type of rec_flag * tt_type_declaration list
  | Tsig_typext of tt_type_extension
  | Tsig_exception of tt_extension_constructor
  | Tsig_module of tt_module_declaration
  | Tsig_recmodule of tt_module_declaration list
  | Tsig_modtype of tt_module_type_declaration
  | Tsig_open of tt_open_description
  | Tsig_include of tt_include_description
  | Tsig_class of tt_class_description list
  | Tsig_class_type of tt_class_type_declaration list
  | Tsig_attribute of tt_attribute

and tt_module_declaration = Base.module_declaration =
    {
     md_id: Ident_visitors.ident_t;
     md_name: string loc;
     md_type: tt_module_type;
     md_attributes: tt_attributes;
     md_loc: Location_visitors.location_t;
    }

and tt_module_type_declaration = Base.module_type_declaration =
    {
     mtd_id: Ident_visitors.ident_t;
     mtd_name: string loc;
     mtd_type: tt_module_type option;
     mtd_attributes: tt_attributes;
     mtd_loc: Location_visitors.location_t;
    }

and tt_open_description = Base.open_description =
    {
     open_path: Path_visitors.path_t;
     open_txt: Longident_visitors.longident_t loc;
     open_override: override_flag;
     open_loc: Location_visitors.location_t;
     open_attributes: tt_attribute list;
    }

and 'a tt_include_infos = 'a Base.include_infos =
    {
     incl_mod: 'a;
     incl_type: (Types_visitors.ty_signature [@opaque]);
     incl_loc: Location_visitors.location_t;
     incl_attributes: tt_attribute list;
    }

and tt_include_description = tt_module_type tt_include_infos

and tt_include_declaration = tt_module_expr tt_include_infos

and tt_with_constraint = Base.with_constraint =
    Twith_type of tt_type_declaration
  | Twith_module of Path_visitors.path_t * Longident_visitors.longident_t loc
  | Twith_typesubst of tt_type_declaration
  | Twith_modsubst of Path_visitors.path_t * Longident_visitors.longident_t loc

and tt_core_type = Base.core_type =
  { mutable ctyp_desc : tt_core_type_desc;
    mutable ctyp_type : (Types_visitors.ty_type_expr [@opaque]);
    ctyp_env : Env_visitors.env_t; (* BINANNOT ADDED *)
    ctyp_loc : Location_visitors.location_t;
    ctyp_attributes: tt_attributes;
   }

and tt_core_type_desc = Base.core_type_desc =
    Ttyp_any
  | Ttyp_var of string
  | Ttyp_arrow of arg_label * tt_core_type * tt_core_type
  | Ttyp_tuple of tt_core_type list
  | Ttyp_constr of
      Path_visitors.path_t * Longident_visitors.longident_t loc
        * tt_core_type list
  | Ttyp_object of (string * tt_attributes * tt_core_type) list * closed_flag
  | Ttyp_class of
      Path_visitors.path_t * Longident_visitors.longident_t loc
        * tt_core_type list
  | Ttyp_alias of tt_core_type * string
  | Ttyp_variant of tt_row_field list * closed_flag * label list option
  | Ttyp_poly of string list * tt_core_type
  | Ttyp_package of tt_package_type

and tt_package_type = Base.package_type = {
  pack_path : Path_visitors.path_t;
  pack_fields : (Longident_visitors.longident_t loc * tt_core_type) list;
  pack_type : (Types_visitors.ty_module_type [@opaque]);
  pack_txt : Longident_visitors.longident_t loc;
}

and tt_row_field = Base.row_field =
    Ttag of label * tt_attributes * bool * tt_core_type list
  | Tinherit of tt_core_type

and tt_value_description = Base.value_description =
  { val_id: Ident_visitors.ident_t;
    val_name: string loc;
    val_desc: tt_core_type;
    val_val: (Types_visitors.ty_value_description [@opaque]);
    val_prim: string list;
    val_loc: Location_visitors.location_t;
    val_attributes: tt_attributes;
    }

and tt_type_declaration = Base.type_declaration =
  {
    typ_id: Ident_visitors.ident_t;
    typ_name: string loc;
    typ_params: (tt_core_type * variance) list;
    typ_type: (Types_visitors.ty_type_declaration [@opaque]);
    typ_cstrs: (tt_core_type * tt_core_type * Location_visitors.location_t) list;
    typ_kind: tt_type_kind;
    typ_private: private_flag;
    typ_manifest: tt_core_type option;
    typ_loc: Location_visitors.location_t;
    typ_attributes: tt_attributes;
   }

and tt_type_kind = Base.type_kind =
    Ttype_abstract
  | Ttype_variant of tt_constructor_declaration list
  | Ttype_record of tt_label_declaration list
  | Ttype_open

and tt_label_declaration = Base.label_declaration =
    {
     ld_id: Ident_visitors.ident_t;
     ld_name: string loc;
     ld_mutable: mutable_flag;
     ld_type: tt_core_type;
     ld_loc: Location_visitors.location_t;
     ld_attributes: tt_attributes;
    }

and tt_constructor_declaration = Base.constructor_declaration =
    {
     cd_id: Ident_visitors.ident_t;
     cd_name: string loc;
     cd_args: tt_constructor_arguments;
     cd_res: tt_core_type option;
     cd_loc: Location_visitors.location_t;
     cd_attributes: tt_attributes;
    }

and tt_constructor_arguments = Base.constructor_arguments =
  | Cstr_tuple of tt_core_type list
      [@name "tt_Cstr_tuple"]
  | Cstr_record of tt_label_declaration list
      [@name "tt_Cstr_record"]
  (* Visitors for these constructors are renamed to avoid clashes with Types *)

and tt_type_extension = Base.type_extension =
  {
    tyext_path: Path_visitors.path_t;
    tyext_txt: Longident_visitors.longident_t loc;
    tyext_params: (tt_core_type * variance) list;
    tyext_constructors: tt_extension_constructor list;
    tyext_private: private_flag;
    tyext_attributes: tt_attributes;
  }

and tt_extension_constructor = Base.extension_constructor =
  {
    ext_id: Ident_visitors.ident_t;
    ext_name: string loc;
    ext_type : (Types_visitors.ty_extension_constructor [@opaque]);
    ext_kind : tt_extension_constructor_kind;
    ext_loc : Location_visitors.location_t;
    ext_attributes: tt_attributes;
  }

and tt_extension_constructor_kind = Base.extension_constructor_kind =
    Text_decl of tt_constructor_arguments * tt_core_type option
  | Text_rebind of Path_visitors.path_t * Longident_visitors.longident_t loc

and tt_class_type = Base.class_type =
    {
     cltyp_desc: tt_class_type_desc;
     cltyp_type: (Types_visitors.ty_class_type [@opaque]);
     cltyp_env: Env_visitors.env_t;
     cltyp_loc: Location_visitors.location_t;
     cltyp_attributes: tt_attributes;
    }

and tt_class_type_desc = Base.class_type_desc =
    Tcty_constr of
      Path_visitors.path_t * Longident_visitors.longident_t loc
        * tt_core_type list
  | Tcty_signature of tt_class_signature
  | Tcty_arrow of arg_label * tt_core_type * tt_class_type

and tt_class_signature = Base.class_signature = {
    csig_self : tt_core_type;
    csig_fields : tt_class_type_field list;
    csig_type : (Types_visitors.ty_class_signature [@opaque]);
  }

and tt_class_type_field = Base.class_type_field = {
    ctf_desc: tt_class_type_field_desc;
    ctf_loc: Location_visitors.location_t;
    ctf_attributes: tt_attributes;
  }

and tt_class_type_field_desc = Base.class_type_field_desc =
  | Tctf_inherit of tt_class_type
  | Tctf_val of (string * mutable_flag * virtual_flag * tt_core_type)
  | Tctf_method of (string * private_flag * virtual_flag * tt_core_type)
  | Tctf_constraint of (tt_core_type * tt_core_type)
  | Tctf_attribute of tt_attribute

and tt_class_declaration = tt_class_expr tt_class_infos

and tt_class_description = tt_class_type tt_class_infos

and tt_class_type_declaration = tt_class_type tt_class_infos

and 'a tt_class_infos = 'a Base.class_infos =
  { ci_virt: virtual_flag;
    ci_params: (tt_core_type * variance) list;
    ci_id_name : string loc;
    ci_id_class: Ident_visitors.ident_t;
    ci_id_class_type : Ident_visitors.ident_t;
    ci_id_object : Ident_visitors.ident_t;
    ci_id_typehash : Ident_visitors.ident_t;
    ci_expr: 'a;
    ci_decl: (Types_visitors.ty_class_declaration [@opaque]);
    ci_type_decl : (Types_visitors.ty_class_type_declaration [@opaque]);
    ci_loc: Location_visitors.location_t;
    ci_attributes: tt_attributes;
   }

[@@deriving
    visitors {
        variety = "iter"
      ; polymorphic = true; monomorphic = ["'env"]
      ; ancestors = [
            "Ident_visitors.iter"
          ; "Env_visitors.iter"
          ; "Types_visitors.iter"
        ]
    }
  , visitors {
        variety = "map"
      ; polymorphic = true; monomorphic = ["'env"]
      ; ancestors = [
            "Ident_visitors.map"
          ; "Env_visitors.map"
          ; "Types_visitors.map"
        ]
    }
  , visitors {
        variety = "reduce"
      ; polymorphic = true; monomorphic = ["'env"]
      ; ancestors = [
            "Ident_visitors.reduce"
          ; "Env_visitors.reduce"
          ; "Types_visitors.reduce"
        ]
      }

    , visitors {
        variety = "iter2"
      ; polymorphic = true; monomorphic = ["'env"]
      ; concrete = true
      ; ancestors = [
            "Ident_visitors.iter2"
          ; "Env_visitors.iter2"
          ; "Types_visitors.iter2"
        ]
    }
]