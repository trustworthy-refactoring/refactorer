
open Compiler

module Base = Longident

type longident_t = Base.t =
    Lident of string
  | Ldot of longident_t * string
  | Lapply of longident_t * longident_t

[@@deriving
    visitors { variety = "iter"; }
  , visitors { variety = "map"; }
  , visitors { variety = "reduce"; }

  , visitors { variety = "iter2"; concrete = true; }
]