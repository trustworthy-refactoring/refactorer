(* --------------------------------------------------------------------------
   Occurrences of the following types in type definitions have been changed
   to use the types indicated in parentheses so that the visitor generation
   code creates calls to the correct method.

        Location.t      (Location_visitors.location_t)
        Longident.t     (Longident_visitors.longident_t)

   All local types (apart from toplevel_phrase and directive_argument) have
   been prefixed with "pt_" as a namespace; this is to avoid clashses in
   generating names for methods visiting the types in the Types and Typedtree
   modules.
   ------------------------------------------------------------------------- *)
open Compiler

open Asttypes

module Base = Parsetree

type pt_constant = Base.constant =
    Pconst_integer of string * char option
  | Pconst_char of char
  | Pconst_string of string * string option
  | Pconst_float of string * char option
and pt_attribute = string loc * pt_payload
and pt_extension = string loc * pt_payload
and pt_attributes = pt_attribute list
and pt_payload = Base.payload =
  | PStr of pt_structure
  | PSig of pt_signature (* : SIG *)
  | PTyp of pt_core_type  (* : T *)
  | PPat of pt_pattern * pt_expression option  (* ? P  or  ? P when E *)
and pt_core_type = Base.core_type =
    {
     ptyp_desc: pt_core_type_desc;
     ptyp_loc: Location_visitors.location_t;
     ptyp_attributes: pt_attributes; (* ... [@id1] [@id2] *)
    }
and pt_core_type_desc = Base.core_type_desc =
  | Ptyp_any
  | Ptyp_var of string
  | Ptyp_arrow of arg_label * pt_core_type * pt_core_type
  | Ptyp_tuple of pt_core_type list
  | Ptyp_constr of Longident_visitors.longident_t loc * pt_core_type list
  | Ptyp_object of (string * pt_attributes * pt_core_type) list * closed_flag
  | Ptyp_class of Longident_visitors.longident_t loc * pt_core_type list
  | Ptyp_alias of pt_core_type * string
  | Ptyp_variant of pt_row_field list * closed_flag * label list option
  | Ptyp_poly of string list * pt_core_type
  | Ptyp_package of pt_package_type
  | Ptyp_extension of pt_extension
and pt_package_type = Longident_visitors.longident_t loc * (Longident_visitors.longident_t loc * pt_core_type) list
and pt_row_field = Base.row_field =
  | Rtag of label * pt_attributes * bool * pt_core_type list
  | Rinherit of pt_core_type
and pt_pattern = Base.pattern =
    {
     ppat_desc: pt_pattern_desc;
     ppat_loc: Location_visitors.location_t;
     ppat_attributes: pt_attributes; (* ... [@id1] [@id2] *)
    }
and pt_pattern_desc = Base.pattern_desc =
  | Ppat_any
  | Ppat_var of string loc
  | Ppat_alias of pt_pattern * string loc
  | Ppat_constant of pt_constant
  | Ppat_interval of pt_constant * pt_constant
  | Ppat_tuple of pt_pattern list
  | Ppat_construct of Longident_visitors.longident_t loc * pt_pattern option
  | Ppat_variant of label * pt_pattern option
  | Ppat_record of (Longident_visitors.longident_t loc * pt_pattern) list * closed_flag
  | Ppat_array of pt_pattern list
  | Ppat_or of pt_pattern * pt_pattern
  | Ppat_constraint of pt_pattern * pt_core_type
  | Ppat_type of Longident_visitors.longident_t loc
  | Ppat_lazy of pt_pattern
  | Ppat_unpack of string loc
  | Ppat_exception of pt_pattern
  | Ppat_extension of pt_extension
  | Ppat_open of Longident_visitors.longident_t loc * pt_pattern
and pt_expression = Base.expression =
    {
     pexp_desc: pt_expression_desc;
     pexp_loc: Location_visitors.location_t;
     pexp_attributes: pt_attributes; (* ... [@id1] [@id2] *)
    }
and pt_expression_desc = Base.expression_desc =
  | Pexp_ident of Longident_visitors.longident_t loc
  | Pexp_constant of pt_constant
  | Pexp_let of rec_flag * pt_value_binding list * pt_expression
  | Pexp_function of pt_case list
  | Pexp_fun of arg_label * pt_expression option * pt_pattern * pt_expression
  | Pexp_apply of pt_expression * (arg_label * pt_expression) list
  | Pexp_match of pt_expression * pt_case list
  | Pexp_try of pt_expression * pt_case list
  | Pexp_tuple of pt_expression list
  | Pexp_construct of Longident_visitors.longident_t loc * pt_expression option
  | Pexp_variant of label * pt_expression option
  | Pexp_record of (Longident_visitors.longident_t loc * pt_expression) list * pt_expression option
  | Pexp_field of pt_expression * Longident_visitors.longident_t loc
  | Pexp_setfield of pt_expression * Longident_visitors.longident_t loc * pt_expression
  | Pexp_array of pt_expression list
  | Pexp_ifthenelse of pt_expression * pt_expression * pt_expression option
  | Pexp_sequence of pt_expression * pt_expression
  | Pexp_while of pt_expression * pt_expression
  | Pexp_for of
      pt_pattern *  pt_expression * pt_expression * direction_flag * pt_expression
  | Pexp_constraint of pt_expression * pt_core_type
  | Pexp_coerce of pt_expression * pt_core_type option * pt_core_type
  | Pexp_send of pt_expression * string
  | Pexp_new of Longident_visitors.longident_t loc
  | Pexp_setinstvar of string loc * pt_expression
  | Pexp_override of (string loc * pt_expression) list
  | Pexp_letmodule of string loc * pt_module_expr * pt_expression
  | Pexp_letexception of pt_extension_constructor * pt_expression
  | Pexp_assert of pt_expression
  | Pexp_lazy of pt_expression
  | Pexp_poly of pt_expression * pt_core_type option
  | Pexp_object of pt_class_structure
  | Pexp_newtype of string * pt_expression
  | Pexp_pack of pt_module_expr
  | Pexp_open of override_flag * Longident_visitors.longident_t loc * pt_expression
  | Pexp_extension of pt_extension
  | Pexp_unreachable
and pt_case = Base.case =
    {
     pc_lhs: pt_pattern;
     pc_guard: pt_expression option;
     pc_rhs: pt_expression;
    }
and pt_value_description = Base.value_description =
    {
     pval_name: string loc;
     pval_type: pt_core_type;
     pval_prim: string list;
     pval_attributes: pt_attributes;  (* ... [@@id1] [@@id2] *)
     pval_loc: Location_visitors.location_t;
    }
and pt_type_declaration = Base.type_declaration =
    {
     ptype_name: string loc;
     ptype_params: (pt_core_type * variance) list;
     ptype_cstrs: (pt_core_type * pt_core_type * Location_visitors.location_t) list;
     ptype_kind: pt_type_kind;
     ptype_private: private_flag;   (* = private ... *)
     ptype_manifest: pt_core_type option;  (* = T *)
     ptype_attributes: pt_attributes;   (* ... [@@id1] [@@id2] *)
     ptype_loc: Location_visitors.location_t;
    }
and pt_type_kind = Base.type_kind =
  | Ptype_abstract
  | Ptype_variant of pt_constructor_declaration list
  | Ptype_record of pt_label_declaration list
  | Ptype_open
and pt_label_declaration = Base.label_declaration =
    {
     pld_name: string loc;
     pld_mutable: mutable_flag;
     pld_type: pt_core_type;
     pld_loc: Location_visitors.location_t;
     pld_attributes: pt_attributes; (* l [@id1] [@id2] : T *)
    }
and pt_constructor_declaration = Base.constructor_declaration =
    {
     pcd_name: string loc;
     pcd_args: pt_constructor_arguments;
     pcd_res: pt_core_type option;
     pcd_loc: Location_visitors.location_t;
     pcd_attributes: pt_attributes; (* C [@id1] [@id2] of ... *)
    }
and pt_constructor_arguments = Base.constructor_arguments =
  | Pcstr_tuple of pt_core_type list
  | Pcstr_record of pt_label_declaration list
and pt_type_extension = Base.type_extension =
    {
     ptyext_path: Longident_visitors.longident_t loc;
     ptyext_params: (pt_core_type * variance) list;
     ptyext_constructors: pt_extension_constructor list;
     ptyext_private: private_flag;
     ptyext_attributes: pt_attributes;   (* ... [@@id1] [@@id2] *)
    }
and pt_extension_constructor = Base.extension_constructor =
    {
     pext_name: string loc;
     pext_kind : pt_extension_constructor_kind;
     pext_loc : Location_visitors.location_t;
     pext_attributes: pt_attributes; (* C [@id1] [@id2] of ... *)
    }
and pt_extension_constructor_kind = Base.extension_constructor_kind =
    Pext_decl of pt_constructor_arguments * pt_core_type option
  | Pext_rebind of Longident_visitors.longident_t loc
and pt_class_type = Base.class_type =
    {
     pcty_desc: pt_class_type_desc;
     pcty_loc: Location_visitors.location_t;
     pcty_attributes: pt_attributes; (* ... [@id1] [@id2] *)
    }
and pt_class_type_desc = Base.class_type_desc =
  | Pcty_constr of Longident_visitors.longident_t loc * pt_core_type list
  | Pcty_signature of pt_class_signature
  | Pcty_arrow of arg_label * pt_core_type * pt_class_type
  | Pcty_extension of pt_extension
and pt_class_signature = Base.class_signature =
    {
     pcsig_self: pt_core_type;
     pcsig_fields: pt_class_type_field list;
    }
and pt_class_type_field = Base.class_type_field =
    {
     pctf_desc: pt_class_type_field_desc;
     pctf_loc: Location_visitors.location_t;
     pctf_attributes: pt_attributes; (* ... [@@id1] [@@id2] *)
    }
and pt_class_type_field_desc = Base.class_type_field_desc =
  | Pctf_inherit of pt_class_type
  | Pctf_val of (string * mutable_flag * virtual_flag * pt_core_type)
  | Pctf_method  of (string * private_flag * virtual_flag * pt_core_type)
  | Pctf_constraint  of (pt_core_type * pt_core_type)
  | Pctf_attribute of pt_attribute
  | Pctf_extension of pt_extension
and 'a pt_class_infos = 'a Base.class_infos =
    {
     pci_virt: virtual_flag;
     pci_params: (pt_core_type * variance) list;
     pci_name: string loc;
     pci_expr: 'a;
     pci_loc: Location_visitors.location_t;
     pci_attributes: pt_attributes;  (* ... [@@id1] [@@id2] *)
    }
and pt_class_description = pt_class_type pt_class_infos
and pt_class_type_declaration = pt_class_type pt_class_infos
and pt_class_expr = Base.class_expr =
    {
     pcl_desc: pt_class_expr_desc;
     pcl_loc: Location_visitors.location_t;
     pcl_attributes: pt_attributes; (* ... [@id1] [@id2] *)
    }
and pt_class_expr_desc = Base.class_expr_desc =
  | Pcl_constr of Longident_visitors.longident_t loc * pt_core_type list
  | Pcl_structure of pt_class_structure
  | Pcl_fun of arg_label * pt_expression option * pt_pattern * pt_class_expr
  | Pcl_apply of pt_class_expr * (arg_label * pt_expression) list
  | Pcl_let of rec_flag * pt_value_binding list * pt_class_expr
  | Pcl_constraint of pt_class_expr * pt_class_type
  | Pcl_extension of pt_extension
and pt_class_structure = Base.class_structure =
    {
     pcstr_self: pt_pattern;
     pcstr_fields: pt_class_field list;
    }
and pt_class_field = Base.class_field =
    {
     pcf_desc: pt_class_field_desc;
     pcf_loc: Location_visitors.location_t;
     pcf_attributes: pt_attributes; (* ... [@@id1] [@@id2] *)
    }
and pt_class_field_desc = Base.class_field_desc =
  | Pcf_inherit of override_flag * pt_class_expr * string option
  | Pcf_val of (string loc * mutable_flag * pt_class_field_kind)
  | Pcf_method of (string loc * private_flag * pt_class_field_kind)
  | Pcf_constraint of (pt_core_type * pt_core_type)
  | Pcf_initializer of pt_expression
  | Pcf_attribute of pt_attribute
  | Pcf_extension of pt_extension
and pt_class_field_kind = Base.class_field_kind =
  | Cfk_virtual of pt_core_type
  | Cfk_concrete of override_flag * pt_expression
and pt_class_declaration = pt_class_expr pt_class_infos
and pt_module_type = Base.module_type =
    {
     pmty_desc: pt_module_type_desc;
     pmty_loc: Location_visitors.location_t;
     pmty_attributes: pt_attributes; (* ... [@id1] [@id2] *)
    }
and pt_module_type_desc = Base.module_type_desc =
  | Pmty_ident of Longident_visitors.longident_t loc
  | Pmty_signature of pt_signature
  | Pmty_functor of string loc * pt_module_type option * pt_module_type
  | Pmty_with of pt_module_type * pt_with_constraint list
  | Pmty_typeof of pt_module_expr
  | Pmty_extension of pt_extension
  | Pmty_alias of Longident_visitors.longident_t loc
and pt_signature = pt_signature_item list
and pt_signature_item = Base.signature_item =
    {
     psig_desc: pt_signature_item_desc;
     psig_loc: Location_visitors.location_t;
    }
and pt_signature_item_desc = Base.signature_item_desc =
  | Psig_value of pt_value_description
  | Psig_type of rec_flag * pt_type_declaration list
  | Psig_typext of pt_type_extension
  | Psig_exception of pt_extension_constructor
  | Psig_module of pt_module_declaration
  | Psig_recmodule of pt_module_declaration list
  | Psig_modtype of pt_module_type_declaration
  | Psig_open of pt_open_description
  | Psig_include of pt_include_description
  | Psig_class of pt_class_description list
  | Psig_class_type of pt_class_type_declaration list
  | Psig_attribute of pt_attribute
  | Psig_extension of pt_extension * pt_attributes
and pt_module_declaration = Base.module_declaration =
    {
     pmd_name: string loc;
     pmd_type: pt_module_type;
     pmd_attributes: pt_attributes; (* ... [@@id1] [@@id2] *)
     pmd_loc: Location_visitors.location_t;
    }
and pt_module_type_declaration = Base.module_type_declaration =
    {
     pmtd_name: string loc;
     pmtd_type: pt_module_type option;
     pmtd_attributes: pt_attributes; (* ... [@@id1] [@@id2] *)
     pmtd_loc: Location_visitors.location_t;
    }
and pt_open_description = Base.open_description =
    {
     popen_lid: Longident_visitors.longident_t loc;
     popen_override: override_flag;
     popen_loc: Location_visitors.location_t;
     popen_attributes: pt_attributes;
    }
and 'a pt_include_infos = 'a Base.include_infos =
    {
     pincl_mod: 'a;
     pincl_loc: Location_visitors.location_t;
     pincl_attributes: pt_attributes;
    }
and pt_include_description = pt_module_type pt_include_infos
and pt_include_declaration = pt_module_expr pt_include_infos
and pt_with_constraint = Base.with_constraint =
  | Pwith_type of Longident_visitors.longident_t loc * pt_type_declaration
  | Pwith_module of Longident_visitors.longident_t loc * Longident_visitors.longident_t loc
  | Pwith_typesubst of pt_type_declaration
  | Pwith_modsubst of string loc * Longident_visitors.longident_t loc
and pt_module_expr = Base.module_expr =
    {
     pmod_desc: pt_module_expr_desc;
     pmod_loc: Location_visitors.location_t;
     pmod_attributes: pt_attributes; (* ... [@id1] [@id2] *)
    }
and pt_module_expr_desc = Base.module_expr_desc =
  | Pmod_ident of Longident_visitors.longident_t loc
  | Pmod_structure of pt_structure
  | Pmod_functor of string loc * pt_module_type option * pt_module_expr
  | Pmod_apply of pt_module_expr * pt_module_expr
  | Pmod_constraint of pt_module_expr * pt_module_type
  | Pmod_unpack of pt_expression
  | Pmod_extension of pt_extension
and pt_structure = pt_structure_item list
and pt_structure_item = Base.structure_item =
    {
     pstr_desc: pt_structure_item_desc;
     pstr_loc: Location_visitors.location_t;
    }
and pt_structure_item_desc = Base.structure_item_desc =
  | Pstr_eval of pt_expression * pt_attributes
  | Pstr_value of rec_flag * pt_value_binding list
  | Pstr_primitive of pt_value_description
  | Pstr_type of rec_flag * pt_type_declaration list
  | Pstr_typext of pt_type_extension
  | Pstr_exception of pt_extension_constructor
  | Pstr_module of pt_module_binding
  | Pstr_recmodule of pt_module_binding list
  | Pstr_modtype of pt_module_type_declaration
  | Pstr_open of pt_open_description
  | Pstr_class of pt_class_declaration list
  | Pstr_class_type of pt_class_type_declaration list
  | Pstr_include of pt_include_declaration
  | Pstr_attribute of pt_attribute
  | Pstr_extension of pt_extension * pt_attributes
and pt_value_binding = Base.value_binding =
  {
    pvb_pat: pt_pattern;
    pvb_expr: pt_expression;
    pvb_attributes: pt_attributes;
    pvb_loc: Location_visitors.location_t;
  }
and pt_module_binding = Base.module_binding =
    {
     pmb_name: string loc;
     pmb_expr: pt_module_expr;
     pmb_attributes: pt_attributes;
     pmb_loc: Location_visitors.location_t;
    }

[@@deriving
    visitors {
        variety = "iter"
      ; polymorphic = true; monomorphic = ["'env"]
      ; ancestors = [
            "Asttype_visitors.iter"
          ; "Longident_visitors.iter"
        ]
    }
  , visitors {
        variety = "map"
      ; polymorphic = true; monomorphic = ["'env"]
      ; ancestors = [
            "Asttype_visitors.map"
          ; "Longident_visitors.map"
        ]
    }
  , visitors {
        variety = "reduce"
      ; polymorphic = true; monomorphic = ["'env"]
      ; ancestors = [
            "Asttype_visitors.reduce"
          ; "Longident_visitors.reduce"
        ]
    }

  , visitors {
        variety = "iter2"
      ; polymorphic = true; monomorphic = ["'env"]
      ; concrete = true
      ; ancestors = [
            "Asttype_visitors.iter2"
          ; "Longident_visitors.iter2"
        ]
    }
]
