(* -------------------------------------------------------------------------- *)
(* Occurrences of the following types in type definitions have been changed   *)
(* to use the types indicated in parentheses so that the visitor generation   *)
(* code creates calls to the correct method.                                  *)
(*      Lexing.position      (Lexing_visitors.position)                       *)
(* -------------------------------------------------------------------------- *)

open Compiler

module Base = Location

type location_t = Base.t = {
  loc_start: Lexing_visitors.position;
  loc_end: Lexing_visitors.position;
  loc_ghost: bool;
}
and 'a loc = 'a Base.loc = {
  txt : 'a;
  loc : location_t;
}

[@@deriving
    visitors { variety = "iter"
             ; polymorphic = true; monomorphic = ["'env"]
             ; ancestors = [ "Lexing_visitors.iter" ]
    }
  , visitors { variety = "map"
             ; polymorphic = true; monomorphic = ["'env"]
             ; ancestors = [ "Lexing_visitors.map" ]
    }
  , visitors { variety = "reduce"
             ; polymorphic = true; monomorphic = ["'env"]
             ; ancestors = [ "Lexing_visitors.reduce" ]
    }

  , visitors { variety = "iter2"
            ; concrete = true
            ; polymorphic = true; monomorphic = ["'env"]
            ; ancestors = [ "Lexing_visitors.iter2" ]
    }
]