
module Opt_reducers = struct

  class ['self] leftmost = object (self : 'self)
      inherit ['self] Typedtree_visitors.reduce
      method zero = None
      method plus x y =
        match x, y with
        | None, _ -> y
        | Some _, _ -> x
    end

  class ['self] rightmost = object (self : 'self)
      inherit ['self] Typedtree_visitors.reduce
      method zero = None
      method plus x y =
        match y, x with
        | None, _ -> x
        | Some _, _ -> y
    end

end