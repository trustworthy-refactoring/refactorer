open Containers
open Fun

open Format

open Compiler
open   Cmt_format
open   Compenv
open   Config

module OrderedKernel = struct
  type t = {
    filename      : string;
    library       : string option ;
    preprocessors : string list ;
  }
  let compare t t' = String.compare t.filename t'.filename
  let equal t t' = String.equal t.filename t'.filename
  let hash t =
    (t.filename, t.library)
      |> (Hash.pair Hash.string (Hash.opt Hash.string))
end

include OrderedKernel

module Set = Set.Make(OrderedKernel)
module Map = Map.Make(OrderedKernel)

let mk (filename, library, preprocessors) =
  if not (Sys.file_exists filename) then raise Not_found ;
  { filename; library; preprocessors; }

module Graph = struct
  module G = Graph.Imperative.Digraph.ConcreteBidirectional(OrderedKernel)
  include G
  module Print = struct
    let node f =
      let open Graph.Gml in [
        "filename", String f.filename ;
        "library",
          List (
            f.library
              |> Option.map_or ~default:[] @@ fun lib
              -> [ "Some", String lib ]) ;
        "preprocessors",
          List (
            f.preprocessors
              |> List.map (fun pp -> "pp", String pp)) ;
      ]
    let edge () = []
  end
  include Graph.Gml.Print(G)(Print)
  module Builder = struct
    module G = G
    let empty = create ~size:0
    let copy = copy
    let add_vertex g v =
      let () = add_vertex g v in g
    let add_edge g v v' =
      let () = add_edge g v v' in g
    let add_edge_e g e =
      let () = add_edge_e g e in g
    let remove_vertex g v =
      let () = remove_vertex g v in g
    let remove_edge g v v' =
      let () = remove_edge g v v' in g
    let remove_edge_e g e =
      let () = remove_edge_e g e in g
  end
  exception ParseError of string
  module Parse = struct
    let edge _ = ()
    let node vl =
      let open Graph.Gml in
      let get = List.Assoc.get_exn ~eq:String.equal in
      let filename = try
        match get "filename" vl with
        | String f -> f
        | _ -> raise (ParseError "Filename is not a string!")
        with Not_found -> raise (ParseError "Cannot find filename!") in
      let lib = try
        match get "library" vl with
        | List [] -> None
        | List [ "Some", String lib ] -> Some lib
        | List _ -> raise (ParseError "Expecting [ \"Some\", String lib ] singleton list!")
        | _ -> raise (ParseError "Non-List value found for Library!")
        with Not_found -> raise (ParseError "Cannot find library!") in
      let pps = try
        match get "preprocessors" vl with
        | List pps ->
          pps
            |> List.map @@ begin function
               | "pp", String pp -> pp
               | _ -> raise (ParseError "Expecting (\"pp\", String pp) for preprocessor!")
               end
        | _ -> raise (ParseError "Preprocessors field is not a list!")
        with Not_found -> raise (ParseError "Cannot find preprocessors!") in
      begin try mk (filename, lib, pps)
      with Not_found -> raise (ParseError (Format.sprintf "File %s not found!" filename))
      end
  end
  include Graph.Gml.Parse(Builder)(Parse)
end

let filename { filename; _ } = filename

let pp_filename fmt { filename; _ } =
  fprintf fmt "%s" filename

let module_name f =
  let f = f.filename in
  Buildenv.module_of_filename f

let cmt_filename f =
  Buildenv.cmt_filename ~lib:f.library ~file:f.filename

let cmt_module_name f =
  let f = cmt_filename f in
  Buildenv.module_of_filename f

let object_filename f =
  Buildenv.object_filename ~lib:f.library ~file:f.filename