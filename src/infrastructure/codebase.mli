open Containers

include Set.S with type elt = Fileinfos.t
(** A codebase is a set of files. *)
(* TODO: Should this interface should enforce some invariants?
    e.g. that [Fileinfos.module_name f] is unique across all [f] in the
         codebase within a given library (cf. type of [find_file]). *)

val add_directory : lib:string option -> ppx:string list -> dir:string -> t -> t
(** Add all appropriate (i.e. OCaml) source files in [dir] to the codebase.
    All files will belong to the library [lib], if provided, and be processed
    using the preprocessors in [ppx].
    Throws a Not_found exception if [dir] cannot be found or accessed.
*)

val find_file : ?lib:string option -> string -> t -> elt option
(** [find_file f fs] will find the first element of [fs] whose filename matches
    [f], in the sense that [f] is a suffix of the element's filename. This
    allows searching for a file in the codebase using just the basename.
    If the optional argument [lib] is specified, the search will additionally
    look for an element in the given library. *)

val find_all_files : string -> t -> elt list
(** [find_all_files f fs] will find all elements in the codebase [fs] with
    filenames that match [f], in the sense that [f] is a suffix of the element's
    filename. *)

val dependency_graph : t -> Fileinfos.Graph.t
(** Calculuates a dependency graph for the files in the given codebase.
    Raises Failure if a typed AST cannot be generated for any file. *)