(** A module encapsulating metadata about a source file. *)

open Containers

type t = private {
  filename      : string ;
  library       : string option ;
  preprocessors : string list ;
}
(** Type representing raw source file in the file system.
    Invariant: file exists at the time of value creation. *)

module Graph : sig
  include Graph.Sig.I with type V.t = t and type V.label = t
  val print : t Format.printer
  (** Pretty-print a fileinfos graph in GML format. *)
  val parse : string -> t
  (** Parse a string in GML format to produce a fileinfos graph. *)
  exception ParseError of string
  (** Exception thrown when parsing fails. *)
end

module Set : Set.S with type elt = t
(** Sets of Fileinfos *)

module Map : Map.S with type key = t
(** Maps from Fileinfos *)

val mk : string * string option * string list -> t
(** Create a value of type [t].
    Throws a Not_found exception if the file does not exist. *)

val compare : t -> t -> int

val equal : t -> t -> bool
(** [equal i i'] returns true if and only if [i] and [i'] represent the same
    physical file. *)

val hash : t Containers.Hash.t
(** Hash function. *)

val filename : t -> string
(** Returns the filename. *)

val pp_filename : t Format.printer
(** Prints the filename of the given fileinfos value. *)

val module_name : t -> string
(** Returns the logical top-level name for the OCaml module defined by this
    file. *)

val cmt_filename : t -> string
(** Returns the basename of .cmt or .cmti file that this source file will be
    compiled to. *)

val cmt_module_name : t -> string
(** Returns the top-level name of the module that this source file will be
    compiled to. *)