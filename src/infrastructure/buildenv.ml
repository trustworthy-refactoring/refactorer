open Containers
open Format

open Compiler

open Longident
open Ident
open Path

open Compenv
open Config

open Ast_lib

type library_name = string
type file_name = string
type module_name = string

let cmt_filename ~lib ~file =
  let fn_no_ext = Filename.remove_extension (Filename.basename file) in
  let ext =
    if (Filename.check_suffix file !interface_suffix)
      then ".cmti" else ".cmt" in
  lib
    |> Option.map_or ~default:(fn_no_ext ^ ext) @@ fun lib_name ->
    (* Reproduce filename mangling used by Jane Street *)
    let prefix = lib_name ^ "__" in
    let module_name = module_of_filename err_formatter file fn_no_ext in
    let fn_no_ext =
      if (String.lowercase_ascii lib_name) =
          (String.lowercase_ascii module_name) then module_name
      else prefix ^ module_name in
    fn_no_ext ^ ext

module Factorise = struct

  let source_info id =
    match String.split ~by:"__" id with
    | [] ->
      (* presumably String.split has the invariant that it returns a non-empty list. *)
      assert false
    | [ x ] ->
      None, x
    | lib :: ss ->
      Some (String.uncapitalize_ascii lib), String.concat "" ss

  let longident id =
    match Longident.flatten id with
    | [] ->
      assert false
    | head :: ids ->
      let lib, head = source_info head in
      let id = List.fold_left (fun lid id -> Ldot(lid, id)) (Lident head) ids in
      lib, id

  let source_info ({ name; _ } as id) =
    let () =
      if not (Ident.persistent id) then
        invalid_arg
          (sprintf "%s.Factorise.source_info" __MODULE__) in
    source_info name

    (* TODO: Fold the call to source_info into the locally defined recursive
          function fix_path, to improve efficiency. Actually, this would be
          folding the call to Path.head. *)
  let path p =
    let lib, head = source_info (Path.head p) in
    let rec fix_path =
      function
      | Pident _ ->
        Pident (Ident.create_persistent head)
      | Pdot (p, id, stamp) ->
        Pdot ((fix_path p), id, stamp)
      | Papply (p, p') ->
        Papply ((fix_path p), p') in
    lib, (fix_path p)

end

let source_info = Factorise.source_info

let rec flatten_path =
  let open Path in
  function
  | Pdot(Pident lib, _mod, _) ->
      let name =
        if String.equal lib.Ident.name (_mod ^ "__")
        then _mod else lib.Ident.name ^ _mod in
      Pident { lib with Ident.name }
  | Pdot (p, s, i) -> Pdot (flatten_path p, s, i)
  | p -> p

let wrap_lookup f ?backup_lib (lib, lid) =
  let lookup_in_lib lib =
    flatten_path
      (f
        (longident_append
          (Lident ((String.capitalize_ascii lib) ^ "__"))
          lid)) in
  let raw_lookup = lazy (f lid) in
  (* If the long identifier to look up is explicitly given to be part of a
     library, then include that library in the lookup path. Otherwise, first try
     looking up the identifier in the backup library if one is given, and if
     this is not given or that lookup fails then fall back on a lookup without a
     library. *)
  match lib, backup_lib with
  | Some lib, _    -> lookup_in_lib lib
  | None, None     -> Lazy.force raw_lookup
  | None, Some lib ->
      try lookup_in_lib lib
      with Not_found -> Lazy.force raw_lookup
