val use : bool Cmdliner.Term.t
(** Command line flag indicating whether information from any .merlin file found
    in the current working directory should be used. *)

val source_dirs : unit -> string list
(** Returns a list of directories containing source files, corresponding to 'S'
    directives in the .merlin file. *)

val include_dirs : unit -> string list
(** Returns a list of directories to be included in the path, corresponding to 
    'B' and 'PKG' directives in the .merlin file. *)