(** A module that computes information about the artifacts produced by a build
    environment (e.g. ocamlbuild, jbuilder, etc.) *)

(* NB. Currently, we just implement the rules enacted by jbuilder to handle
       libraries. *)

open Compiler

type library_name = string
type file_name = string
type module_name = string

val cmt_filename : lib:library_name option -> file:file_name -> string
(** [cmt_filename ~lib ~file] returns the basename of .cmt or .cmti file that
    the source [file] will be compiled to as part of the [lib] library. *)

val source_info : Ident.t -> library_name option * module_name
(** [source_info id] returns the source-level module name and library
    corresponding to the build-level module name contained in the identifier
    [id]. *)

val wrap_lookup :
  (Longident.t -> Path.t) -> ?backup_lib:library_name
    -> (library_name option * Longident.t) -> Path.t
(** [wrap_lookup f ] takes a compiler lookup function [f] that transforms a
    long identifier into a path (perhaps obtained as a closure over one of the
    [Env.lookup_] functions), TODO *)

(*val longident_of_id : Identifier.t -> Longident.t*)
(** Converts an internal identifier to a [Longident.t]. This function lives here
    since the conversion depends on the build environment to turn library
    information (which the compiler is not aware of) into identifier
    information understood by the compiler. *)

module Factorise : sig

  val path : Path.t -> library_name option * Path.t
  (** [factorise_path p] will extract the library from the compiler path [p], and
      also return the path without the head component corresponding to the
      library.
      Note, the function will raise Invalid_arg if the path [p] is not a
      persistent (i.e. top-level) path. Also, it returns a persistent path, but
      this will not correspond to a valid path as returned by a lookup, due to
      the build scheme.

      TODO: What is the right solution for the return path? *)

  val longident : Longident.t -> library_name option * Longident.t

end
