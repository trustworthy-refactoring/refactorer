open Containers

open Lib

let source_dirs = ref []
let include_dirs = ref []

let use =
  let docs = Cmdliner.Manpage.s_common_options in
  let doc = "Don't take information from a .merlin file \
             (this is the default behaviour)." in
  let arg_info = Cmdliner.Arg.info ~docs ~doc ["no-merlin"] in
  Cmdliner.(Term.app (Term.const not) Arg.(value & flag arg_info))


(* **********
   The following code is taken from the merlin source code. Since merlin does
   not expose any of its functionality as a library, for now we have to
   duplicate the parts of its source code that we need.
   ********** *)

(*  Taken from file src/utils/std.ml *)
module Glob : sig
  type pattern =
    | Wildwild
    | Exact of string
    | Regexp of Str.regexp
  val compile_pattern : string -> pattern
  val match_pattern : pattern -> string -> bool
end = struct
  type pattern =
    | Wildwild
    | Exact of string
    | Regexp of Str.regexp

  let compile_pattern = function
    | "**" -> Wildwild
    | pattern ->
      let regexp = Buffer.create 15 in
      let chunk = Buffer.create 15 in
      let flush () =
        if Buffer.length chunk > 0 then (
          Buffer.add_string regexp (Str.quote (Buffer.contents chunk));
          Buffer.clear chunk;
        )
      in
      let l = String.length pattern in
      let i = ref 0 in
      while !i < l do
        begin match pattern.[!i] with
          | '\\' -> incr i; if !i  < l then Buffer.add_char chunk pattern.[!i]
          | '*' -> flush (); Buffer.add_string regexp ".*";
          | '?' -> flush (); Buffer.add_char regexp '.';
          | x -> Buffer.add_char chunk x
        end;
        incr i
      done;
      if Buffer.length regexp = 0 then
        Exact (Buffer.contents chunk)
      else (
        flush ();
        Buffer.add_char regexp '$';
        Regexp (Str.regexp (Buffer.contents regexp))
      )

  let match_pattern re str =
    match re with
    | Wildwild -> true
    | Regexp re -> Str.string_match re str 0
    | Exact s -> String.equal s str
end

(* Taken from src/ocaml/utils/misc.ml *)

let rec split_path path acc =
  match Filename.dirname path, Filename.basename path with
  | dir, _ when String.equal dir path -> dir :: acc
  | dir, base -> split_path dir (base :: acc)

let canonicalize_filename ?cwd path =
  let parts =
    match split_path path [] with
    | dot :: rest when String.equal dot Filename.current_dir_name ->
      split_path (match cwd with None -> Sys.getcwd () | Some c -> c) rest
    | parts -> parts
  in
  let goup path = function
    | dir when String.equal dir Filename.parent_dir_name ->
      (match path with _ :: t -> t | [] -> [])
    | dir when String.equal dir Filename.current_dir_name ->
      path
    | dir -> dir :: path
  in
  let parts = List.rev (List.fold_left goup [] parts) in
  let filename_concats = function
    | [] -> ""
    | root :: subs -> List.fold_left Filename.concat root subs
  in
  filename_concats parts
  
let rec expand_glob ~filter acc root = function
  | [] -> root :: acc
  | Glob.Wildwild :: _tl -> (* FIXME: why is tl not used? *)
    let rec append acc root =
      let items = try Sys.readdir root with Sys_error _ -> [||] in
      let process acc dir =
        let filename = Filename.concat root dir in
        if filter filename
        then append (filename :: acc) filename
        else acc
      in
      Array.fold_left process (root :: acc) items
    in
    append acc root
  | Glob.Exact component :: tl ->
    let filename = Filename.concat root component in
    expand_glob ~filter acc filename tl
  | pattern :: tl ->
    let items = try Sys.readdir root with Sys_error _ -> [||] in
    let process acc dir =
      if Glob.match_pattern pattern dir then
        let root' = Filename.concat root dir in
        if filter root' then
          expand_glob ~filter acc root' tl
        else acc
      else acc
    in
    Array.fold_left process acc items

let expand_glob ?(filter=fun _ -> true) path acc =
  match split_path path [] with
  | [] -> acc
  | root :: subs ->
    let patterns = List.map Glob.compile_pattern subs in
    expand_glob ~filter acc root patterns

(* ********** END OF DUPLICATED MERLIN CODE ********** *)

let () =
  try Findlib.init ()
  with exn ->
    let message = match exn with
      | Failure message -> message
      | exn -> Printexc.to_string exn in
    Logging.err @@ fun _f -> _f 
      "Error during findlib initialization: %s" message

let add_path path ref =
  let path = String.trim path in
  let path = canonicalize_filename path in
  ref := expand_glob path !ref

let () =
  let cwd = Sys.getcwd () in
  let file = Filename.concat cwd ".merlin" in
  if Sys.file_exists file then
    let () = Logging.info @@ fun _f -> _f "Reading merlin file %s" file in
    IO.with_in file @@ fun file ->
    IO.read_lines_l file |> List.iter @@ fun line ->
    let line = String.trim line in
    match String.chop_prefix ~pre:"S " line with
    | Some path ->
      add_path path source_dirs
    | _ ->
    match String.chop_prefix ~pre:"B " line with
    | Some path ->
      add_path path include_dirs
    | _ ->
    match String.chop_prefix ~pre:"CMT " line with
    | Some path ->
      add_path path include_dirs
    | _ ->
    match String.chop_prefix ~pre:"PKG " line with
    | Some pkg ->
      begin try
        let path = Findlib.package_directory (String.trim pkg) in
        include_dirs := path :: !include_dirs
      with Findlib.No_such_package (pkg, msg) ->
        Logging.err @@ fun _f -> _f
          "Findlib failed to retrieve path of package %s: %s" pkg msg
      end
    | _ ->
      ()

let source_dirs () = !source_dirs
let include_dirs () = !include_dirs