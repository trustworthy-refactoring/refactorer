open Lazy

open Containers
open String
open Format
open Fun
open IO

open Compiler

open Misc
open Config
open Compenv
open Compmisc

open Lexing
open Location

open Logging.Tags

open Compiler_utils
open Lib

open Fileinfos

type ast =
  | Interface of Parsetree.signature option * Typedtree.signature option
  | Implementation of Parsetree.structure option * Typedtree.structure option

type t = {
  fileinfos   : Fileinfos.t ;
  contents    : string ;
  ast         : ast ;
}

(* Cache for Sourcefile.t values *)
module Cache = Hashtbl.Make(Fileinfos)
let cache = Cache.create 100

let mk_lexer in_channel filename =
  let buf = Lexing.from_channel in_channel in
  let () = buf.Lexing.lex_curr_p <- {
    buf.Lexing.lex_curr_p with Lexing.pos_fname = filename } in
  buf

let parse_implementation in_channel f =
  let buf = mk_lexer in_channel f.filename in
  let _struct = Parse.implementation buf in
  let tool_name = !Configuration.tool_name in
  Clflags.all_ppx := f.preprocessors ;
  Pparse.apply_rewriters_str ~restore:false ~tool_name _struct

let parse_interface in_channel f =
  let buf = mk_lexer in_channel f.filename in
  let _sig = Parse.interface buf in
  let tool_name = !Configuration.tool_name in
  Clflags.all_ppx := f.preprocessors ;
  Pparse.apply_rewriters_sig ~restore:false ~tool_name _sig

let do_typing pt =
  let env = initial_env () in
  try match pt with
  | `None intf ->
      if intf then Interface (None, None)
              else Implementation (None, None)
  | `Intf (pt, f) ->
      Interface (Some pt, Some (Typemod.type_interface f env pt))
  | `Impl (pt, f) ->
      let opref = output_prefix f in
      let module_name = module_of_filename err_formatter f opref in
      let tt, _ =
        Typemod.type_implementation f opref module_name env pt in
      Implementation (Some pt, Some tt)
  with Typetexp.Error _ as err ->
    let () = log_error (error_of_exn err) in
    match pt with
    | `None _ -> assert false
    | `Intf _ -> Interface (None, None)
    | `Impl _ -> Implementation (None, None)

let environment_fixer = object
  inherit [_] Typedtree_visitors.map
  method! visit_env_t env this =
    Envaux.env_of_only_summary this
end

let to_ast infos in_channel =
  let f = infos.filename in
  let intf = Filename.check_suffix f !interface_suffix in
  let pt =
    try if intf
      then `Intf ((parse_interface in_channel infos), f)
      else `Impl ((parse_implementation in_channel infos), f)
    with Syntaxerr.Error _ as err ->
      let () = log_error (error_of_exn err) in
      `None intf in
  try
    let cmt_file =
      find_in_path_uncap !Config.load_path (cmt_filename infos) in
    let () = Logging.debug @@ fun _f -> _f "Reading %s" cmt_file in
    let cmt_info = Cmt_format.read_cmt cmt_file in
    let () = Envaux.reset_cache () in
    match pt, cmt_info.Cmt_format.cmt_annots with
    | `Impl _, Cmt_format.Interface _
    | `Intf _, Cmt_format.Implementation _ ->
        failwith "Cmt format does not match parsetree!"
    | _, Cmt_format.Interface tt ->
        let tt = environment_fixer#visit_tt_signature () tt in
        begin match pt with
        | `None true -> Interface (None, Some tt)
        | `Intf (pt, _) -> Interface (Some pt, Some tt)
        | _ -> assert false
        end
    | _, Cmt_format.Implementation tt ->
        let tt = environment_fixer#visit_tt_structure () tt in
        begin match pt with
        | `None false -> Implementation (None, Some tt)
        | `Impl (pt, _) -> Implementation (Some pt, Some tt)
        | _ -> assert false
        end
    | _,_ ->
        let () = Logging.warn @@ fun _f -> _f
          "cmt file contains unexpected payload" in
        do_typing pt
  with
  | Not_found ->
      let () = Logging.warn @@ fun _f -> _f "No .cmt file found." in
      do_typing pt
  | Cmt_format.Error (Cmt_format.Not_a_typedtree s) ->
      let () = Logging.warn @@ fun _f -> _f "Error reading cmt file: %s" s in
      do_typing pt
  | Envaux.Error (Envaux.Module_not_found p) ->
    (* N.B. Only the one constructor for [Envaux.error] in 4.04.0 *)
      let () = Logging.warn @@ fun _f -> _f
        ~header:"Envaux Error" "Module lookup failed for %a" Printtyp.path p in
      do_typing pt

let to_ast infos =
  with_in infos.filename (to_ast infos)

let of_fileinfos ?(use_cache=true) fileinfos =
  if use_cache && Cache.mem cache fileinfos then
    Cache.find cache fileinfos
  else
    let contents = with_in fileinfos.filename read_all in
    let ast = to_ast fileinfos in
    let sf = { fileinfos; contents; ast; } in
    let () = if use_cache then Cache.add cache fileinfos sf in
    sf

let process_ast ~intf_f ~impl_f f =
  match f.ast with
  | Implementation (pt, tt) -> impl_f (pt, tt)
  | Interface (pt, tt) -> intf_f (pt, tt)

include (struct
  open Typedtree
  class virtual ['self] propagate_envs = object (self : 'self)
    inherit ['self] Typedtree_visitors.reduce as super
    method! visit_tt_pattern _ pat =
      super#visit_tt_pattern (Some pat.pat_env) pat
    method! visit_tt_expression _ e =
      super#visit_tt_expression (Some e.exp_env) e
    method! visit_tt_class_expr _ e =
      super#visit_tt_class_expr (Some e.cl_env) e
    method! visit_tt_module_expr _ e =
      super#visit_tt_module_expr (Some e.mod_env) e
    method! visit_tt_structure_item _ item =
      super#visit_tt_structure_item (Some item.str_env) item
    method! visit_tt_module_type _ t =
      super#visit_tt_module_type (Some t.mty_env) t
    method! visit_tt_signature_item _ item =
      super#visit_tt_signature_item (Some item.sig_env) item
    method! visit_tt_core_type _ t =
      super#visit_tt_core_type (Some t.ctyp_env) t
    method! visit_tt_class_type _ t =
      super#visit_tt_class_type (Some t.cltyp_env) t
  end
end)

module Set : Containers.Set.S with type elt = Module.name_t =
  Containers.Set.Make(String)

let get_module_deps =
  let visitor = object(self)
      inherit [_] propagate_envs as super
      method zero = Set.empty
      method plus = Set.union
      method! visit_path_t env p =
        let env = Option.get_exn env in
        Env.normalize_path None env p
          |> Path.heads
          |> List.filter Ident.persistent
          |> List.map Ident.name
          |> Set.of_list
    end in
  fun f ->
    let proj_tt (_, tt) =
      tt
      |> Option.get_lazy
          (fun () -> raise (Invalid_argument "No typed AST!")) in
    f
    |> process_ast
          ~intf_f:(proj_tt %> (visitor#visit_tt_signature None))
          ~impl_f:(proj_tt %> (visitor#visit_tt_structure None))
    |> Set.to_list

let initial_module_scope f =
  match f.ast with
  | Implementation _ ->
      invalid_arg (Format.sprintf "%s.initial_module_scope" __MODULE__)
  | Interface (_, Some _sig) -> ((module_name f.fileinfos), Some _sig), []
  | _ -> failwith "No typed AST for module signature!"

let extract_src =
  let err_string = Format.sprintf "%s.extract_src: bad positions!" __MODULE__ in
  fun f l l' ->
  let extract start stop =
    let src = f.contents in
    if stop >= length src then
      invalid_arg err_string
    else
      let slice = Sub.make src start (stop - start) in
      try Sub.copy slice
      with Invalid_argument s ->
        let err_string = Format.sprintf "%s.extract_src: %s" __MODULE__ s in
        invalid_arg err_string in
  match l, l' with
  | { loc_start = s; loc_end = e; _ },
    { loc_start = s'; loc_end = e'; _ }
      when s.pos_cnum <= s'.pos_cnum && e'.pos_cnum <= e.pos_cnum ->
      (* l' contained within l *)
      extract s.pos_cnum s'.pos_cnum
  | { loc_end; _ }, { loc_start; _ }
      when loc_end.pos_cnum <= loc_start.pos_cnum ->
      (* l' starts after l finishes *)
      extract loc_end.pos_cnum loc_start.pos_cnum
  | _ -> invalid_arg err_string

let diff f s =
  let (in_channel, out_channel) as channels =
    Unix.open_process
      (Format.sprintf
        "diff -u \"%s\" - | filterdiff --remove-timestamps"
        (f.fileinfos.filename)) in
  output_string out_channel s ;
  (* Need to close the channel before diff outputs anything *)
  close_out out_channel ;
  let result = read_all in_channel in
  match Unix.close_process channels with
  | Unix.WEXITED 0 ->
    result
  | Unix.WEXITED code ->
    failwith
      (Format.sprintf
        "%s.diff: subcommand exited with code %i" __MODULE__ code)
  | Unix.WSIGNALED signal ->
    failwith
      (Format.sprintf
        "%s.diff: subcommand killed by signal %i" __MODULE__ signal)
  | Unix.WSTOPPED signal ->
    failwith
      (Format.sprintf
        "%s.diff: subcommand stopped by signal %i" __MODULE__ signal)
