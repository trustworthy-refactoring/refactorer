open Containers

open Compiler

open Asttypes
open Location
open Lexing

open Typedtree

open Lib

let location_to_string = mk_to_string Location.print
let location_to_string_compact = mk_to_string Location.print_compact

let longident_to_string = mk_to_string Printtyp.longident

let path_to_string = mk_to_string Printtyp.path

let cmp_loc
    { loc_start = { pos_cnum = loc1; _ }; _ }
    { loc_start = { pos_cnum = loc2; _ }; _ } =
  loc1 - loc2

let cmp_by_loc e e' = cmp_loc e.exp_loc e'.exp_loc

let is_arg_pun preamble = function
  | Labelled lbl, { exp_desc = Texp_ident (_, { txt; _ }, _); _ } ->
      lbl = (longident_to_string txt) &&
      not (String.mem (strip_comments preamble) ~sub:(Tokens.mk_arg_label lbl))
  | Optional lbl,
    { exp_desc = Texp_construct (_, { Types.cstr_name; _},
        [ { exp_desc = Texp_ident (_, { txt; _ }, _); _ } ] ); _ }
        when cstr_name = "Some" ->
      lbl = (longident_to_string txt)
  | _ -> false

let is_field_pun preamble _ =
  not (String.contains (strip_comments preamble) '=')


let build_longident xs =
  List.fold_left
    (fun p m -> Longident.Ldot (p, m))
    (Longident.Lident (List.hd xs))
    (List.tl xs)

let longident_hd lid = List.hd (Longident.flatten lid)
let longident_tl lid = build_longident (List.tl (Longident.flatten lid))

let rec longident_append lid =
  let open Longident in
  function
  | Lident id -> Ldot(lid, id)
  | Ldot(lid', id) -> Ldot(longident_append lid lid', id)
  | Lapply _ -> invalid_arg (Format.sprintf "%s.longident_append" __MODULE__)

let rec path_drop acc p p' =
  if Path.same p p' then
    Some (build_longident acc)
  else
    match p' with
    | Path.Pident _ -> None
    | Path.Pdot (p', id, _) -> path_drop (id::acc) p p'
    | Path.Papply _ ->
      invalid_arg "path_drop: Papply not yet supported!"

let path_drop p p' = path_drop [] p p'

let path_dest_ident p =
  match p with
  | Path.Pident id -> id
  | _ ->
    invalid_arg (Format.sprintf "Ast_utils.path_dest_ident: %a" Printtyp.path p)

let rec path_to_longident =
  let open Path in
  let open Longident in
  function
  | Pident { Ident.name; _ } -> Lident name
  | Pdot (p, id, _) -> Ldot (path_to_longident p, id)
  | Papply (p, p') -> Lapply (path_to_longident p, path_to_longident p')
