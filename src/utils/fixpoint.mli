module type S = sig
  type elt
  type t
  val next_fp : t -> (elt -> t) -> t
end

module OfSet(X : Set.S) : S with type elt = X.elt and type t = X.t