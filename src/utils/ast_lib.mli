open Containers

open Compiler
open Asttypes

val location_to_string : Location.t -> string
val longident_to_string : Longident.t -> string
val path_to_string : Path.t -> string

val cmp_loc : Location.t -> Location.t -> int

val cmp_by_loc : Typedtree.expression -> Typedtree.expression -> int

val check_loc : Location.t -> string Array.t -> bool

val is_arg_pun : string -> arg_label * Typedtree.expression -> bool

val is_field_pun :
  string
    -> Types.label_description * (Longident.t loc * Typedtree.expression)
    -> bool
