open Containers

open Compiler

open Elements
open Identifier

open Typedtree_views

type ('a, 'b) binding_source =
  'b item_list_ctxt * 'b item_ctxt * 'b item_description_ctxt *
    [ `Binding of ('a, 'b) item_element
    | `Include of      'b  include_view
    ]

(* val pp_binding_source : ('a, 'b) binding_source Format.printer *)

val next_env : ('a, 'b) binding_source -> Env.t
val next_items : ('a, 'b) binding_source -> 'b items_view

val find_binding :
  (Ident.t, 'a) Atom.t -> 'b root_view -> ('a, 'b) binding_source option

val find_path_to_binding :
  (Ident.t, 'a) Atom.t -> string * 'b root_view -> Identifier.Chain._t Option.t
