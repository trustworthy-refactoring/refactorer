open Compiler

open Driver
open Compmisc
open Compenv

open Parsing

open Typing
module TT = Typedtree

open Frontend

open Format

let () = Arg.parse speclist (fun _ -> ()) usage

module Visitors : sig
  val visit_struct : Typedtree.structure -> unit
  val visit_sig : Typedtree.signature -> unit
end = struct
  let visitor = object
      inherit [_] Typedtree_visitors.iter as super
      method! visit_tt_module_expr env this =
        begin match this.Typedtree.mod_desc with
        | TT.Tmod_ident (path, _) ->
            print_endline (sprintf "Tmod_ident: %s" 
              (Path.name (Env.normalize_path (Some this.TT.mod_loc) this.TT.mod_env path)))
        | TT.Tmod_structure _ ->
            print_endline "Tmod_structure"
        | TT.Tmod_constraint (expr, _, constr, coercion) ->
            print_endline (sprintf "Tmod_constraint (%s, %s)"
              (match constr with 
                | TT.Tmodtype_implicit -> "Tmod_implicit" 
                | TT.Tmodtype_explicit _ -> "Tmod_explicit")
              (match coercion with
                | TT.Tcoerce_none -> "Tcoerce_none"
                | TT.Tcoerce_structure (_,_) -> "Tcoerce_structure"
                | TT.Tcoerce_functor (_,_) -> "Tcoerce_function"
                | TT.Tcoerce_primitive _ -> "Tcoerce_primitive"
                | TT.Tcoerce_alias (_,_) -> "Tcoerce_alias"))
        | _ -> ()
        end ;
        super#visit_tt_module_expr env this
      method! visit_tt_module_binding env this =
        print_endline (sprintf "Module binding to %s" this.TT.mb_id.Ident.name) ;
        super#visit_tt_module_binding env this
      method! visit_Tstr_include env decl =
        print_endline "Module include" ;
        super#visit_Tstr_include env decl
    end
  let visit_struct = visitor#visit_tt_structure ()
  let visit_sig = visitor#visit_tt_signature ()
end

let () = 
  let input = create_input () in
  init_path false ;
  Env.set_unit_name input.Frontend.module_name ;
  try
    Frontend.process_input input 
      (fun _struct -> 
        let _struct, _, _ = 
          Typemod.type_toplevel_phrase (initial_env ()) _struct in
        Visitors.visit_struct _struct) 
      (fun _sig -> 
        let _sig = Typemod.type_interface input.filename (initial_env ()) _sig in
        Visitors.visit_sig _sig)
  with Typetexp.Error (loc, env, err) -> 
    Typetexp.report_error env Format.err_formatter err ;
    prerr_newline () ;
    Location.print Format.err_formatter loc ;
    exit 1
