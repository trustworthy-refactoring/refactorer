#! /bin/bash

rlwrap ocaml \
  -init utils/init.ml \
  -I _build/src/ast_visitors \
  -I _build/src/compiler \
  -I _build/src/driver \
  -I _build/src/infrastructure \
  -I _build/src/language \
  -I _build/src/refactorings \
  -I _build/src/refactorings/rename \
  -I _build/src/utils \
  -I _build/utils \
  $*
