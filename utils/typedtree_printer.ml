open Format

open Rotor
open Compiler

open Compmisc

open Frontend
open Sourcefile
open Fileinfos

open Logging

open Containers.Fun

let flags =
  let untype_flag =
    let doc = "Convert the typed AST into a parse tree." in
    let info = Cmdliner.Arg.info ~doc ["untype"] in
    (`Untype, info) in
  let untype_pp_flag =
    let doc = "Convert the typed AST into a parse tree and pretty print." in
    let info = Cmdliner.Arg.info ~doc ["untype"] in
    (`UntypePP, info) in
  Cmdliner.Arg.(value & vflag `Typed [untype_flag; untype_pp_flag])

let run codebase opt =
  let print_intf, print_impl =
    match opt with
    | `Typed ->
      Printtyped.interface,
      Printtyped.implementation
    | `Untype ->
      (fun fmt -> Untypeast.untype_signature %> Printast.interface fmt),
      (fun fmt -> Untypeast.untype_structure %> Printast.implementation fmt)
    | `UntypePP ->
      (fun fmt -> Untypeast.untype_signature %> Pprintast.signature fmt),
      (fun fmt -> Untypeast.untype_structure %> Pprintast.structure fmt) in
  codebase |> Codebase.iter @@ of_fileinfos %> (fun f ->
  print_endline f.fileinfos.filename ;
  match f.ast with
  | Interface (_, Some _sig) ->
      print_intf std_formatter _sig
  | Implementation (_, Some _struct) ->
      print_impl std_formatter _struct
  | _ ->
      prerr_endline "No AST found!")

let cmd =
  let open Cmdliner.Term in
  (with_common_opts (const run $ (Codebase.of_cmdline $ const !Configuration.tool_name) $ flags)),
  (info "Parsetree Printer")

let () =
  Cmdliner.Term.(exit @@ eval cmd)
