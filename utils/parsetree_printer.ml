open Format

open Compiler

open Compmisc

open Frontend
open Sourcefile
open Fileinfos

open Logging

open Containers.Fun

module type Printer =
sig
  val interface : Format.formatter -> Parsetree.signature -> unit
  val implementation: Format.formatter -> Parsetree.structure -> unit
end

let printer = ref (module Printast : Printer)

let speclist =
  Frontend.speclist @
  Logging.speclist @
  [
    "-pp", Arg.Unit
      (fun () ->
        printer := (module (struct
            let interface = Pprintast.signature
            let implementation = Pprintast.structure
          end) : Printer)),
      ": Pretty print the AST"
  ]

let () = Arg.parse speclist (fun _ -> ()) usage

let () =
  Logs.set_reporter (Logging.reporter ()) ;
  Logs.set_level !Logging.log_level

let () =
  init_path false ;
  let codebase = Lazy.force codebase in
  let () =
      if Codebase.cardinal codebase = 0
        then failwith "No input file specified!" in
  codebase |> Codebase.iter @@ of_fileinfos %> (fun f ->
  print_endline f.fileinfos.filename ;
  let module Printer = (val !printer : Printer) in
  match f.ast with
  | Interface (Some _sig, _) ->
      Printer.interface std_formatter _sig
  | Implementation (Some _struct, _) ->
      Printer.implementation std_formatter _struct
  | _ ->
      prerr_endline "No AST found!")
