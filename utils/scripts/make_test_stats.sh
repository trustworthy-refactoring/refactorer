if [ -d stats ]; then
  rm -fR stats
fi
mkdir stats

N_SUCC=0
for f in *.patch; do
  if [ -f $f ] && [ ! -f "${f%.patch}.failed" ]; then
    # increase success counter
    ((N_SUCC++))
    # The renamed identifier
    echo -n "${f%.patch}, ";
    # The number of files affected in the patch
    echo -n "`lsdiff -n $f | wc -l`";
    # The total number of hunks in the patch
    echo -n ", `lsdiff -nv $f | grep -P "^\t" | wc -l`";
    # The number of dependencies
    if [ -f "${f%.patch}.deps" ]; then
      echo ", `grep -E '^\S' \"${f%.patch}.deps\" | wc -l`";
    else
      echo ""
    fi
  fi
done > stats/success-cases-stats.csv

N_FAIL=0
for f in *.patch; do
  if [ -f $f ] && [ -f "${f%.patch}.failed" ]; then
    # increase failure counter
    ((N_FAIL++))
    # The renamed identifier
    echo -n "${f%.patch}, ";
    # The number of files affected in the patch
    echo -n "`lsdiff -n $f | wc -l`";
    # The total number of hunks in the patch
    echo -n ", `lsdiff -nv $f | grep -P "^\t" | wc -l`";
    # The number of dependencies
    if [ -f "${f%.patch}.deps" ]; then
      echo ", `grep -E '^\S' \"${f%.patch}.deps\" | wc -l`";
    else
      echo ""
    fi
  fi
done > stats/failure-cases-stats.csv

for f in *.error; do
  if [ -f $f ]; then
    echo -n "${f%.error}, ";
    ERROR_TEXT=$(grep Fatal $f | sed 's/"/""/g');
    echo "\"$ERROR_TEXT\"";
  fi
done > stats/error-list.csv

# ouput success ratio
N_TOTAL=$((N_SUCC+N_FAIL))
echo "($N_SUCC/$N_TOTAL) successful refactorings"
