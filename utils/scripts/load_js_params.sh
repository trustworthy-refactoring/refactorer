#!/bin/bash

OCAML_PATH=$(opam config var lib || echo -n "")

if [ -n "$OCAML_PATH" ]; then
    OCAML_PATH=$((ocamlfind printconf path || echo -n "") | (read dir; echo -n "$dir"))
fi

# Generate include options

INCLUDE_PARAMS=""

while read INCLUDE || [[ -n "$INCLUDE" ]]; do
    INCLUDE_PARAMS="$INCLUDE_PARAMS -I $OCAML_PATH/$INCLUDE"
done < ./test/jane-street/include-ocaml

while read INCLUDE || [[ -n "$INCLUDE" ]]; do
    INCLUDE_PARAMS="$INCLUDE_PARAMS -I $INCLUDE"
done < ./test/jane-street/include-js

#Generate input options

INPUT_DIRS=""

while read INPUTDIR LIB || [[ -n "$INPUTDIR" ]]; do
    INPUT_DIRS="$INPUT_DIRS -d $INPUTDIR"
    if [[ -n "$LIB" ]]; then
        INPUT_DIRS="$INPUT_DIRS -lib $LIB"
    fi
done < ./test/jane-street/libs

export INCLUDE_PARAMS
export INPUT_DIRS
