#!/bin/bash

# The file containing the identifiers to run tests over is expected as the first
# parameter of the script. We check that it is accessible

if [ $# -lt 1 ]; then
  echo "Please specify a file containing the test cases to be run!"
  exit 1
fi

TEST_CASE_FILE=$1

if [ ! -f "$TEST_CASE_FILE" ]; then
  echo "Cannot find test case file!"
  exit 1
fi

# The directory in which to store the results is expected as the second
# parameter of the script. We check that it is accessible.

if [ $# -lt 2 ]; then
  echo "Please specify a directory for storing the results!"
  exit 1
fi

RESULTS_DIR=$2

if [ ! -d "$RESULTS_DIR" ]; then
  echo "Results directory does not exist!"
  exit 1
fi

# The third, optional parameter to the script is the number of cores available
# for running the test case refactorings in parallel. This number is passed to
# xargs using the -P option.

CORES=$3

if [ -z "$CORES" ]; then
  CORES=1
fi

# We need to know where the testbed is.

if [ ! -d "$JANE_STREET_PATH" ]; then
  echo "Cannot find Jane Street test bed!"
  exit 1
fi

# Keep a record of the base directory.

BASE_DIR=`pwd`

# Check the the refactoring executable exists

EXE="$BASE_DIR/rotor"

if [ ! -x "$EXE" ]; then
  echo "Cannot find main executable!"
  exit 1
fi

# Export these variables so they are available in the run_test function that is
# called in a subshell by xargs, below
export EXE
export BASE_DIR
export RESULTS_DIR

# This sets the INCLUDE_PARAMS and INPUT_DIRS variables which are used to pass
# parameters to the refactoring executable in the run_test function below.
source ./utils/scripts/load_js_params.sh

# This function runs one refactoring test case
function run_test {

  ID=$1

  CHAIN=$(./utils/toplevel.sh ./utils/scripts/factorise_identifier.ml ${ID})

  if [ -z "$CHAIN" ]; then
    return 0
  fi

  OUTPUT=$(cd "$JANE_STREET_PATH" && \
    "$EXE" $INCLUDE_PARAMS $INPUT_DIRS \
      -get-deps-from-file "$BASE_DIR/test/jane-street/filedeps" \
      -log-file "$RESULTS_DIR/$ID.log" \
      -rdeps "$RESULTS_DIR/$ID.deps" \
      -r rename $CHAIN foo 2>&1)

  if [ $? -ne 0 ]; then

    echo "$OUTPUT" > "$RESULTS_DIR/$ID.error"
    echo "$ID REFACTORING_FAILED"

  else

    echo "$OUTPUT" > "$RESULTS_DIR/$ID.patch"

  fi

}

# Export this function so that it is available in the subshell called by xargs, below
export -f run_test

# Generate a list of the identifiers that do not correspond to operators, to be
# piped to xargs below

IDS=""
XARGS_IDS=""
while read ID COUNT || [[ -n "$ID" ]]; do
  if [[ "$ID" =~ ^[\._a-zA-Z\']*$ ]]; then
    IDS="$IDS $ID"
    # Add quotes around each ID and separate with \n for passing to xargs
    XARGS_IDS="$XARGS_IDS\n\"$ID\""
  fi
done < "$TEST_CASE_FILE"

# Run the refactorings through xargs to take advantage of parallelism
echo -e $XARGS_IDS | xargs -P$CORES -I {} bash -c 'run_test "{}"'

# Now go through the results, and check the build status of all the refactoring
# test cases that didn't fail

# We don't run these test cases in parallel with xargs because jbuilder already
# detects and uses available parallelism.

if [ -z "$REFACTOR_ONLY" ]; then

  cd "$RESULTS_DIR"

  for ID in $IDS; do
    PATCH_FILE="$ID.patch"
    if [ -f "$PATCH_FILE" ]; then
      "$BASE_DIR/utils/scripts/test_build.sh" "$PATCH_FILE"
    fi
  done

fi

# Now generate statistics
cd "$RESULTS_DIR"
"$BASE_DIR/utils/scripts/make_test_stats.sh"

# Return to the base directory
cd "$BASE_DIR"