#use "./utils/init.ml" ;;

if Array.length Sys.argv < 2 then
  exit 0 ;;

let lid =
  Longident.parse Sys.argv.(1) ;;

match Buildenv.Factorise.longident lid with
| None, _ ->
  ()
| lib, lid ->
  match List.rev (Longident.flatten lid) with
  | [] ->
    ()
  | v :: atoms ->
    let v = Identifier.Atom.(Ex (mk Elements.Base.Value (Data.only v))) in
    let atoms =
      List.map
        (fun m ->
          Identifier.Atom.(Ex (mk Elements.Base.Structure (Data.only m))))
        (atoms) in
    let id = (lib, (Identifier.Chain.build (v::atoms))) in
    Containers.Format.fprintf Containers.Format.std_formatter
      "%a" Identifier._pp id
