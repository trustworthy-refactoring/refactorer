#use "./utils/init.ml" ;;

if Array.length Sys.argv < 2 then
  exit 0 ;;

let lid =
  Longident.parse Sys.argv.(1) ;;

let lib, lid = Buildenv.Factorise.longident lid in
match List.rev (Longident.flatten lid) with
| [] ->
  assert false
| v :: atoms ->
  let v = Identifier.Atom.(Ex (mk Elements.Base.Value (Data.only v))) in
  let atoms =
    match atoms with
    | [] ->
      [ Identifier.Atom.(Ex (mk Elements.Base.Structure (Data.only "Pervasives"))) ]
    | _ ->
      List.map
        (fun m ->
          Identifier.Atom.(Ex (mk Elements.Base.Structure (Data.only m))))
        (atoms) in
  let id = (lib, (Identifier.Chain.build (v::atoms))) in
  Containers.Format.fprintf Containers.Format.std_formatter
    "%a" Identifier._pp id
