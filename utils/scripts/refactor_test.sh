#!/bin/bash

# The file containing the identifiers to run tests over is expected as the first
# parameter of the script. We check that it is accessible

if [ $# -lt 1 ]; then
  echo "Please specify test cases to be run!"
  exit 1
fi

ID=$1

# Keep a record of the base directory.
BASE_DIR=$(pwd)

RESULTS_DIR="$BASE_DIR/tmp"

# Check the the refactoring executable exists
EXE="$BASE_DIR/rotor"

if [ ! -x "$EXE" ]; then
  echo "Cannot find main executable!"
  exit 1
fi

# This sets the INCLUDE_PARAMS and INPUT_DIRS variables which are used to pass
# parameters to the refactoring executable in the run_test function below.
source ./utils/scripts/load_js_params.sh

# This function runs one refactoring test case

CHAIN=$(./utils/toplevel.sh ./utils/scripts/factorise_identifier.ml "${ID}")

if [ -z "$CHAIN" ]; then
  exit 0
fi

OUTPUT=$(cd "$JANE_STREET_PATH" && \
"$EXE" $INCLUDE_PARAMS $INPUT_DIRS \
  -get-deps-from-file "$BASE_DIR/test/jane-street/filedeps" \
  -log-file "$RESULTS_DIR/$ID.log" \
  -rdeps "$RESULTS_DIR/$ID.deps" \
  -r rename $CHAIN foo 2>&1)

if [ $? -ne 0 ]; then
  echo "Refactoring failed"
  echo "$OUTPUT" > "$RESULTS_DIR/$ID.error"
  exit 1
fi

echo "Refactoring successful"
PATCH_FILE="$RESULTS_DIR/$ID.patch"
echo "$OUTPUT" > "$PATCH_FILE"

