#use "topfind";;

#require "unix";;

#require "devcompiler";;
#require "devcompiler.ocaml_utils";;
#require "devcompiler.ocaml_parsing";;
#require "devcompiler.ocaml_typing";;
#require "devcompiler.ocaml_driver";;

#require "visitors";;
#require "visitors.runtime";;

#require "containers";;
#require "ocamlgraph";;
#require "mparser";;
#require "mparser.pcre";;
#require "logs";;

#load "refactor_lib.cma";;

open Compiler;;

(* Some useful utility functions *)

let load_paths ?(prefix="") f =
  List.iter
    (fun dir ->
      Clflags.include_dirs :=
        (prefix ^ dir)
          :: !Clflags.include_dirs)
  (Containers.IO.with_in f Containers.IO.read_lines_l)
;;
